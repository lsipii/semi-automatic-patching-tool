<?php
require_once('class.Stuff.php');
class GenerateStuff extends Stuff
{
	private $patch_base_version = null;
	private $patch_folder = null;
	
	function __construct($current_shop, $filename_called_from_ajax = null, $initial_call = false)
	{
		$this->initial_patch_call = $initial_call;

		foreach (Config::$patches as $variable => $value)
		{
			if (property_exists(__CLASS__, $variable))
			{
				$this->$variable = $value;					
			}
		}
		if (!$this->patch_folder)
		{
			if (!$this->patch_base_version || empty(Config::$general_operation_info["upgrade_to_version"]))
			{
				throw new Exception(__CLASS__.": Some required constructor params are undefined..");	
			}
			else
			{
				$patch_name = $this->patch_base_version.Config::$general_operation_info["upgrade_to_version"];
				$mainVersionNumber = substr($this->patch_base_version,0,3);
				$this->patch_folder = Config::$general_operation_info["base_versions_directory"].VQMod::$directorySeparator.$mainVersionNumber.VQMod::$directorySeparator.$patch_name;
				if (isset(Config::$general_operation_info["base_filepath_prefix"]))
	            {
	                $this->patch_folder .= VQMod::$directorySeparator.Config::$general_operation_info["base_filepath_prefix"];
	            }
	            if (!file_exists($this->patch_folder))
	            {
	            	throw new Exception("The patch could not be found, tried:  $this->patch_folder", 1);
	            }

			}
		}
		$this->current_working_directory = Config::$patches['working_directory'].VQMod::$directorySeparator.$current_shop.VQMod::$directorySeparator; 
		Config::$shops[$current_shop]["shop_version"] = $current_shop;

		parent::__construct($current_shop);
		$this->initStuff(__CLASS__, $filename_called_from_ajax);
		
		if (!$filename_called_from_ajax)
		{
			// Clear previous generations
			//$this->DeleteStuffFromStuff($this->current_working_directory);

			// Create working directory structure from template
			$working_directory_template = "core".VQMod::$directorySeparator."templates".VQMod::$directorySeparator."src".VQMod::$directorySeparator;
			$this->CopyStuffToStuff($working_directory_template, $this->current_working_directory);
			
			
			if ($this->initial_patch_call)
			{
				// Cheat the Stuff class by calling the patch a "tailoring" and making the "new" and "base" copies of the other.	
				// Copy base version to the "new" directory
				$this->CopyStuffToStuff($this->base_version_folder, $this->new_version_folder);
			
				// Copy patch to the initial patch working directory
				$this->CopyStuffToStuff($this->patch_folder, $this->tailored_version_folder);

				// If there was new files in the patch folder, copy to the traversing new folder
				$this->CopyStuffToStuff($this->patch_folder, $this->new_version_folder);


			}
			else
			{
				//$this->CopyStuffToStuff($this->base_version_folder, $this->new_version_folder);
				
				// Copy filenames from the inital patch, from the current "base" to the current "new" and "tailorings" directory
				$initial_patch_upgraded_files_folder = Config::$patches["working_directory"].VQMod::$directorySeparator.Config::$patches["patch_base_version"].VQMod::$directorySeparator.Config::$general_operation_info["upgrade_folder"].VQMod::$directorySeparator;
				$this->TraverseFolder($initial_patch_upgraded_files_folder, "CopyPatchedFilenameFromBaseToWorkingDirectory");
			}	
		}
		else
		{
			// Copy the rules from inital patch to the current working directory
			$initial_patch_xml_folder = Config::$patches["working_directory"].VQMod::$directorySeparator.Config::$patches["patch_base_version"].VQMod::$directorySeparator.VQMod::$vqXmlPath;
			$current_version_xml_folder = $this->current_working_directory.VQMod::$directorySeparator.VQMod::$vqXmlPath;
			$replaces = array($this->patch_base_version => $this->current_shop);
			$replace_also_content = true;
			$this->CopyStuffToStuff($initial_patch_xml_folder, $current_version_xml_folder, $replaces, $replace_also_content);
		}
	}
	function __destruct()
	{
		unset(Config::$shops[$this->current_shop]);
		parent::__destruct();
	}
	public function CopyPatchedFilenameFromBaseToWorkingDirectory($fileinfo)
	{
		$filepath_patch = $fileinfo->getPathname(); // Patched file
		$filepath_parent_folder = $fileinfo->getPath();
		$initial_patch_upgraded_files_folder = Config::$patches["working_directory"].VQMod::$directorySeparator.Config::$patches["patch_base_version"].VQMod::$directorySeparator.Config::$general_operation_info["upgrade_folder"].VQMod::$directorySeparator;
		
		$filepath_new = str_replace($initial_patch_upgraded_files_folder, $this->new_version_folder, $filepath_patch); 
		$filepath_new_parent_folder = str_replace($initial_patch_upgraded_files_folder, $this->new_version_folder, $filepath_parent_folder);

		$filepath_base = str_replace($initial_patch_upgraded_files_folder, $this->base_version_folder, $filepath_patch);
		$filepath_tailorings = str_replace($initial_patch_upgraded_files_folder, $this->tailored_version_folder, $filepath_patch);
		$filepath_tailorings_parent_folder = str_replace($initial_patch_upgraded_files_folder, $this->tailored_version_folder, $filepath_parent_folder);

		$filepath_upgrade_folder = str_replace($initial_patch_upgraded_files_folder, $this->upgrade_folder, $filepath_patch);
		$filepath_upgrade_parent_folder = str_replace($initial_patch_upgraded_files_folder, $this->upgrade_folder, $filepath_parent_folder);
		
		if ($filepath_upgrade_parent_folder == $filepath_parent_folder)
		{
			$filepath_upgrade_parent_folder = "./";
		}
		if ($filepath_tailorings_parent_folder == $filepath_parent_folder)
		{
			$filepath_tailorings_parent_folder = "./";
		}
		if ($filepath_new_parent_folder == $filepath_parent_folder)
		{
			$filepath_new_parent_folder = "./";
		}

		$clear_filepath = str_replace($initial_patch_upgraded_files_folder, "", $filepath_patch);
		$clear_filepath_parent_folder = str_replace($initial_patch_upgraded_files_folder, "", $filepath_parent_folder);


		if ($fileinfo->isDir())
		{
			$this->EnsureDirs($filepath_upgrade_folder);
			$this->EnsureDirs($filepath_new);
			$this->EnsureDirs($filepath_tailorings);
		}
		else 
		{
			$this->EnsureDirs($filepath_upgrade_parent_folder);
			$this->EnsureDirs($filepath_tailorings_parent_folder);
			$this->EnsureDirs($filepath_new_parent_folder);

			$success = false;
			if (file_exists($filepath_base))
			{
				$success = $this->CopyFile($filepath_base, $filepath_new);
			}
			else
			{
				$success = $this->CopyFile($filepath_patch, $filepath_new);
			}
			$success = $this->CopyFile($filepath_patch, $filepath_tailorings);
			
			if ($success === false)
			{
				echo $filepath_patch.".. exists: ".file_exists($filepath_patch)."<br>";
				echo $clear_filepath."<br>";
				echo $filepath_base.".. exists: ".file_exists($filepath_base)."<br>";
				echo $filepath_tailorings.".. exists: ".file_exists($filepath_tailorings)."<br>";
				echo $filepath_new.".. exists: ".file_exists($filepath_tailorings)."<br>";
				echo $filepath_upgrade_folder.".. exists: ".file_exists($filepath_tailorings)."<br>";
				throw new exception("copypatchedfilenamefrombasetoworkingdirectory failed to copy to $filepath_new");
			}
			return $success;
		}

		return;
	}
	public function overrideDiffContainerTemplate($template_data = array())
	{
		$template_data["caller"] = __CLASS__; // For AjaxHandler
		$template_data["base_version_file_header_text"] = "Base $this->shop_version";
		$template_data["new_version_file_header_text"] = "Base $this->shop_version";
		$template_data["tailored_version_file_header_text"] = "Patched ".Config::$patches["patch_base_version"];
		$template_data["upgraded_version_file_header_text"] = "Saved $this->new_version";

		if (empty($template_data["errors"]))
		{
			if (file_exists($template_data["upgraded_filename"]))
			{
				$template_data["file_message"] = "Committed file:";
				$template_data["file_controls"] =" ----- <a href='' class='file_control RevertDiff'>Revert the changes from the upgrade folder</a>"; 	
			}
			else if (file_exists($template_data["upgrade_cache_new_file"]))
			{
				if (!isset($template_data["file_message"]))
				{
					$template_data["file_message"] = "";
				}
				$template_data["file_message"] .= "VQMod: show generated file:";
				$template_data["file_controls"] =", commit changes to the upgrade folder: <a href='' class='file_control CommitDiff'>Commit</a>"; 	
			}
			else
			{
				$template_data["file_message"] = "VQMod: ".$template_data["new_filename"]." not generated..:";
			}
		}
		parent::printDiffContainerTemplate($template_data);
	}
	public function ClearGenerations($params)
	{
		// Truncate the patch working directory
		$current_working_directory = Config::$patches['working_directory'].VQMod::$directorySeparator.$params['version'].VQMod::$directorySeparator;
		$success = Stuff::DeleteStuffFromStuff($current_working_directory);
		
		// If something was saved already, truncate the thing!
		/* Can't do before checking if the file was committed, and maybe pushed in the upgrade view
		$upgrade_dir = Config::$upgrade['working_directory'].VQMod::$directorySeparator.$params['version'].VQMod::$directorySeparator.Config::$general_operation_info['new_version_folder'].VQMod::$directorySeparator;
		$success = Stuff::DeleteStuffFromStuff($upgrade_dir);
		*/

		$data = array();
		$data['success'] = $success;
		$data['message'] = ($success) ? "" : "Erasing $delete_folder just somehow failed";
		echo json_encode($data);
	}
	public function GetAllTheDiffs($post)
	{

		$new = $post['new_filename'];
		$base = $post['base_filename'];
		$tailored = $post['tailored_filename'];
		$upgraded = $post['upgraded_filename'];
		$generated = $post['upgrade_cache_new_file'];

		$new_header_text = $post['new_version_file_header_text'];
		$base_header_text = $post['base_version_file_header_text'];
		$tailored_header_text = $post['tailored_version_file_header_text'];
		$upgraded_header_text = $post['upgraded_version_file_header_text'];
		$generated_header_text = $post['upgrade_cache_new_file_header_text'];

		$tailored = str_replace($post['current_shop'], Config::$patches["patch_base_version"], $tailored);
		
		$messages = "";
		$tables_html = "<br><br>";

		if (file_exists($base) && file_exists($tailored))
		{
			
			if (file_exists($upgraded))
			{
				$diff = Diff::compareFiles($base, $upgraded);
				$tables_html .= Diff::toTable($diff, '', "", $base_header_text, $upgraded_header_text);
				unset($diff);
				$diff = null;
			}
			else if (file_exists($generated))
			{
				$messages .= "No saved (committed) patch<br>";
				
				// Base versus patch
				$diff = Diff::compareFiles($base, $tailored);
				$tables_html .= Diff::toTable($diff, '', "", $base_header_text, $tailored_header_text);
				unset($diff);
				$diff = null;

				// Base (new) versus generated
				$diff = Diff::compareFiles($new, $generated);
				$tables_html .= Diff::toTable($diff, '', "", $new_header_text, $generated_header_text);
				unset($diff);
				$diff = null;
			}
			else
			{
				$messages .= "No generated file<br>";
			}
		}
		else
		{
			$messages .= "<br> Can not make the diff.. No base and/or tailored files..";
		}

		$data = array();
		$data['message'] = $messages;
		$data['result'] = $tables_html;
		$data['success'] = true;
		echo json_encode($data);
	}

	public function FetchTailoredFile($post)
	{
		$plain_filename = $post['plain_filename'];
		$tailored_filename = $post['tailored_filename'];
		$upgrade_cache_new_file = $post['upgrade_cache_new_file'];
		$current_shop = $post['current_shop'];
		$success = false;

		$upgrade_stuff = new $this->caller($current_shop, $upgrade_cache_new_file);
		$upgrade_stuff->vqcreator->setFilesToCheck(array($plain_filename));
		$upgrade_stuff->vqcreator->CheckMods(); // Throws
				
		$file_contents = file_get_contents($upgrade_cache_new_file);
		if (($file_contents) === false)
		{
			throw new Exception(__CLASS__."::FetchTailoredFile() Could not read file contents: $upgrade_cache_new_file");
		}
		$file_contents = self::RemoveTmpVariables($file_contents);
		
		if ((file_put_contents($tailored_filename, $file_contents) !== false))
		{	
			$success = true;
		}

		$upgrade_stuff->setModifiedFiles(array($plain_filename));
		ob_start();
		$upgrade_stuff->DiDeDiffTadleDonDerrorDilesDplease();
		$new_diff_line = ob_get_clean();

		$upgrade_stuff = null;
		unset($upgrade_stuff);

		$data['new_diff_row_html'] = $new_diff_line;
		$data['message'] = ($success) ? "Wrote $tailored_filename" : "Failed to write $tailored_filename";
		$data['success'] = $success;
		echo json_encode($data);
	}
}
?>
