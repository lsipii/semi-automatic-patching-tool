<?php
class VQModXMLGenerator
{
	function __construct($params = array(), $filename_called_from_ajax = null)
	{	
		foreach ($params as $variable => $value)
		{
			if (property_exists(__CLASS__, $variable))
			{
				$this->$variable = $value;					
			}
		}
		if ($params['shop_version'] && $params['new_version'])
		{
			$this->upgrade_version_info = $params['shop_version']."_".$params['new_version'];
		}
		if (!$this->upgrade_version_info || !$this->author || !$this->current_working_directory)
		{
			throw new Exception(__CLASS__.": Some required params are undefined..");	
		}
		
		$this->vqmod_version = VQMod::$_vqversion;
		$this->vqmod_xml_path = $this->current_working_directory.VQMod::$vqXmlPath;
		$this->vqmod_cache_file = $this->current_working_directory.VQMod::$modCache;
		$this->vqmod_file_cache_path = $this->current_working_directory.VQMod::$vqCachePath;
		$this->vqmod_file_log_path = $this->current_working_directory.VQMod::$logFolder;
		$this->patch_vqmod_xml_path =  Config::$patches["working_directory"].VQMod::$directorySeparator.$params['shop_version'].VQMod::$directorySeparator.VQMod::$vqXmlPath;
		$this->new_version_folder = $this->new_version_folder.VQMod::$directorySeparator;

		// Clear cache
		if ($filename_called_from_ajax)
		{
			//$this->ClearVQModCacheFile($filename_called_from_ajax);
		}
		else
		{
			$this->ClearVQModCache();	
		}	
	}

	// VQMod version info and paths
	private $vqmod_version = null;
	private $vqmod_xml_path = null;
	private $vqmod_cache_file = null;
	private $vqmod_file_cache_path = null;
	private $vqmod_file_log_path = null;
	private $patch_vqmod_xml_path = null;

	// Upgrade information
	private $upgrade_version_info = null;
	private $current_working_directory = null;
	private $new_version_folder = null;
	private $author = null;
	private $additional_info;

	// Temporary data
	private $upgrade_id = 0;
	private $file_modified = false;
	private $last_unmodified_line = "";
	private $search_line_indexes = array();
	private $offset_from_last_unmodified_line = 0;
	private $filename = null;
	private $xml_strings = array();
	private $filestocheck = array();
	private $type = null;
	private $current_flow_has_tailorings = false;
	function setFilesToCheck($files = array())
	{
		$this->filestocheck = $files;
	}
	function setFilename($filename)
	{
		$this->filename = $filename;
		$this->file_modified = false;
		$this->last_unmodified_line = "";
		$this->search_line_indexes = array();
		$this->offset_from_last_unmodified_line = 0;
		$this->xml_strings = array();
	}
	/* 
	 * Generates xml search rules from the diff data arrays
	 * @param bool $modified if there are differences in the current file comparison
	 * @param array $last_unmodifieds a list of last unmodified lines 
	 * @param array $last_deletes a list of last deleted lines
	 * @param array $last_inserts a list of added lines
	 */
	function AppendRules($modified, $last_unmodifieds = array(), $last_deletes = array(), $last_inserts = array())
	{
		/*var_dump($modified, $last_unmodifieds, $last_deletes, $last_inserts);
		echo "<br>";
		var_dump($this->last_unmodified_line);
		echo "<br><br>";*/
		if ($modified)
		{
			$this->file_modified = true;
			$this->upgrade_id++;

			if (false && $this->upgrade_id == "4" && $this->filename == "admin/reports/payment_report.php")
			{
				var_dump($last_unmodifieds);
				var_dump($last_deletes);
				var_dump($last_inserts);
			}
		}
		
		/*
		 * Prepare some search data from last unmodified lines
		 */
		if (count($last_unmodifieds))
		{
			$this->offset_from_last_unmodified_line = 0;	
			foreach ($last_unmodifieds as $key => $value) 
			{
				// Only add unmodified lines to search params if it contains non-whitespace contents and is not a single char
				$test_value = trim($value);
				if (strlen($test_value) > 1)
				{
					$this->last_unmodified_line = $value;
					if (!isset($this->search_line_indexes["$this->last_unmodified_line"]))
					{
						$this->search_line_indexes["$this->last_unmodified_line"] = 0;
					}
					$this->search_line_indexes["$this->last_unmodified_line"]++;
					$this->offset_from_last_unmodified_line = 0;
				}
				else
				{
					$this->offset_from_last_unmodified_line++;
				}
			}
		}
		
		/*
		 * If lines are deleted
		 */
		if ($linecount = count($last_deletes))
		{
			$position = "replace";
			$search = $last_deletes[0];

			$offset = $linecount-1;
			$add = "";
			
			// If the first deleted line was empty we have to fetch the search rule from last unmodified lines.
			$test_value = trim($search);
			if ($test_value == "")
			{
				
				// If the operation is at the top of the file.
				if (empty($this->last_unmodified_line))
				{
					$position = "top";	
					$search = "";
					$index = 0;
				}
				else
				{
					// Instead of just deleteting, overwrite the line with the last unmodified line.
					// This might drop some empty lines from the generated file
					$search = $this->last_unmodified_line;
					$add = $this->last_unmodified_line;
					$index = $this->search_line_indexes["$this->last_unmodified_line"];
				}	
			}
			else
			{
				if (!isset($this->search_line_indexes["$search"]))
				{
					$this->search_line_indexes["$search"] = 0;
				}
				$this->search_line_indexes["$search"]++;
				$index = $this->search_line_indexes["$search"];
			}
			/*
			 * Or if lines are replaced with new lines
			 */
			if ($insert_lint_count = count($last_inserts))
			{
				// Mark the lines with post and prefixes for colouring
				$tailoring_lines = preg_grep("/tailoring/i", $last_inserts);
				foreach ($tailoring_lines as $i => $line)
				{
					if ($this->current_flow_has_tailorings)
					{
						$this->current_flow_has_tailorings = false;
						$last_inserts[$i] = $line.Stuff::$tailored_lines_end;
					}
					else
					{
						$this->current_flow_has_tailorings = true;
						$last_inserts[$i] = Stuff::$tailored_lines_start.$line;
					}
				}
	 			$add .= Stuff::$modified_line_prefix.implode("\n", $last_inserts).Stuff::$modified_line_postfix;
	 		}
	 		
			$this->xml_strings[$this->upgrade_id] = $this->CreateVQModXMLOperationString($position, $search, $add, $index, $offset);
		}
		/* 
		 * Or if lines are inserted 
		 */
		else if (count($last_inserts))
		{
			if (empty($this->last_unmodified_line))
			{
				$position = "top";	
				$search = "";
				$index = 0;
				$offset = 0;
			}
			else
			{
				$position = "after";
				$search = $this->last_unmodified_line;
				$index = $this->search_line_indexes["$this->last_unmodified_line"];
				$offset = $this->offset_from_last_unmodified_line;
			}	
			
			// Mark the lines with post and prefixes for colouring
			$tailoring_lines = preg_grep("/tailoring/i", $last_inserts);
			foreach ($tailoring_lines as $i => $line)
			{
				if ($this->current_flow_has_tailorings)
				{
					$this->current_flow_has_tailorings = false;
					$last_inserts[$i] = $line.Stuff::$tailored_lines_end;
				}
				else
				{
					$this->current_flow_has_tailorings = true;
					$last_inserts[$i] = Stuff::$tailored_lines_start.$line;
				}
			}
 			$add = Stuff::$modified_line_prefix.implode("\n", $last_inserts).Stuff::$modified_line_postfix;
 			
 			$this->xml_strings[$this->upgrade_id] = $this->CreateVQModXMLOperationString($position, $search, $add, $index, $offset);
		}
	}	
	function Commit()
	{
		if ($this->filename == null)
		{
			throw new Exception(__CLASS__.'::'.__FUNCTION__.': No filename');
		}

		if ($this->file_modified)
		{
			$file_string = str_replace(VQMod::$directorySeparator, "-", $this->filename);
			$file_string = str_replace(".", "-", $file_string);
			$vqmod_cache_filename = $file_string."_".$this->upgrade_version_info.".xml";

			$vqmod_xml_filename = $this->vqmod_xml_path.$vqmod_cache_filename;
			$vqmod_xml_contents = $this->CreateVQModXMLString();

			if (Config::$general_operation_info["upgrade_or_patch"] == "patch" && $this->type == "UpgradeStuff")
			{
				$vqmod_xml_contents = $this->CheckForSimilarReplacesInPatchFile($vqmod_cache_filename, $vqmod_xml_contents);
			}

			if ((file_put_contents($vqmod_xml_filename, $vqmod_xml_contents) === false))
			{
				throw new Exception(__CLASS__.'::'.__FUNCTION__.': Error writing file: '.$vqmod_xml_filename);	
			}
			else
			{
				// Add filename to modcheck-list
				$this->filestocheck[] = $this->filename;
			}
		}
	}
	/*
	 * If we're done generating patches and this run is a shop version upgrade, the search rules
	 * related to replaces must be checked if the patch already replaced contents. The upgrade check
	 * will fail if the search value was already replaced in the patch. This function syncronizes
	 * the search rule.
	 */ 
	private function CheckForSimilarReplacesInPatchFile($xml_filename, $contents)
	{
		$content_xml = simplexml_load_string($contents, null, LIBXML_NOCDATA);
		$content_search = $content_xml->file[0]->operation->search;
		$content_operation = (string) $content_search["position"];
		$content_search = (string) $content_search;
		$content_add = (string) $content_xml->file[0]->operation->add;

		$patch_xml_filename = $this->patch_vqmod_xml_path.$xml_filename;
		$patch_content_xml = simplexml_load_file($patch_xml_filename, null, LIBXML_NOCDATA);
		foreach ($patch_content_xml->file[0] as $operation)
		{
			$patch_content_search = $operation->search;
			$patch_content_operation = (string) $patch_content_search["position"];

			// If the patch has replaced our search parameters
			if ($patch_content_operation == "replace")
			{
				$patch_content_search = (string) $patch_content_search;
				$patch_content_add = (string) $operation->add;
				$patch_content_add = Stuff::RemoveTmpVariables($patch_content_add);

				if (strpos($content_search, $patch_content_search) !== false)
				{
					$contents = preg_replace("/\>(.*)<\/search\>/", "><![CDATA[$patch_content_add]]></search>", $contents);
				}
			}
		}
		return $contents;
	}

	public $vqmod_errors = array();
	function CheckMods()
	{
		VQMod::bootup($this->current_working_directory, true, $this->new_version_folder);
		foreach ($this->filestocheck as $key => $filename)
		{
			// Generate file template
			VQMod::modCheck($filename);
		}
		$this->vqmod_errors = VQMod::GetErrors();
		$this->filestocheck = array();
	}
	private function ClearVQModCacheFile($cache_filename)
	{	
		if (file_exists($cache_filename))
		{
			unlink($cache_filename);
		}
	}
	private function ClearVQModCache()
	{
		if (file_exists($this->vqmod_cache_file))
		{ 
			unlink($this->vqmod_cache_file);
		}
		foreach (glob($this->vqmod_file_cache_path.'*.php') as $filename) 
		{
		    unlink($filename);
		}
		foreach (glob($this->vqmod_xml_path.'*.xml') as $filename) 
		{
		    unlink($filename);
		}
		foreach (glob($this->vqmod_file_log_path.'*.log') as $filename) 
		{
		    unlink($filename);
		}
	}
	private function ClearVQModSession()
	{

	}	
	private function CreateVQModXMLString()
	{
		$filename = $this->new_version_folder.$this->filename;
		//echo $filename;
		//echo $this->new_version_folder;

		$main_xml = "<!-- Generated by VQModXMLGenerator : Temporary file -->
<modification>
	<id><![CDATA[".$this->upgrade_version_info."-".$this->upgrade_id."]]></id>
	<version><![CDATA[".$this->upgrade_version_info."]]></version>
	<vqmver><![CDATA[".$this->vqmod_version."]]></vqmver>
	<author><![CDATA[".$this->author."]]></author>
	<file name=\"".$filename."\" path=\"\">";

		foreach ($this->xml_strings as $key => $xml_string)
		{		
			$main_xml .= $xml_string;
		}

		$main_xml .= "
	</file>
</modification>
<!-- Generated by VQModXMLGenerator : Temporary file -->";

		return $main_xml;
	}
	private function CreateVQModXMLOperationString($position, $search, $add, $index = 0, $offset = 0)
	{
		//$index = "2";
		$upgrade_operation_info = "Upgrade: ".$this->upgrade_version_info."-".$this->upgrade_id." :: ". $this->filename ." @".$this->author;
		$upgrade_operation_info .= $this->additional_info;

		// This index stuff works in a really strange way when applying the vqmod..
		$this->search_line_indexes["$search"]--;

		$xml_string = "
		<operation info=\"".$upgrade_operation_info."\">
			<search position=\"".$position."\" trim=\"false\"";
		if ($offset)
		{
			$xml_string .= " offset=\"".$offset."\" ";
		}
		if ($index)
		{
			$xml_string .= " index=\"".$index."\" ";
		}
		$xml_string .= "><![CDATA[".$search."]]></search>
			<add trim=\"false\"><![CDATA[".$add."]]></add>
		</operation>";
	
		return $xml_string;
	}
}
?>