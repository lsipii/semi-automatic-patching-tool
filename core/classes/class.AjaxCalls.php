<?php

class AjaxCalls
{
	public $upgrade_params;
	public $connection_params;

	public function GetFinalDiff($post)
	{
		$new = $post['new_filename'];
		$old = $post['base_filename'];
		$new_header_text = $post['new_version_file_header_text'];
		$old_header_text = $post['base_version_file_header_text'];
		
		if (file_exists($generated))
		{
			$diff = Diff::compareFiles($new, $generated);
			$tables_html .= Diff::toTable($diff, '', "", $new_header_text, $generated_header_text);
			unset($diff);
			$diff = null;
		}
		echo $tables_html;
	}
	public function GetAllTheDiffs($post)
	{

		$new = $post['new_filename'];
		$base = $post['base_filename'];
		$tailored = $post['tailored_filename'];
		$upgraded = $post['upgraded_filename'];
		$generated = $post['upgrade_cache_new_file'];

		$new_header_text = $post['new_version_file_header_text'];
		$base_header_text = $post['base_version_file_header_text'];
		$tailored_header_text = $post['tailored_version_file_header_text'];
		$upgraded_header_text = $post['upgraded_version_file_header_text'];
		$generated_header_text = $post['upgrade_cache_new_file_header_text'];

		$messages = "";
		$tables_html = "<br><br>";

		if (file_exists($base) && file_exists($tailored))
		{
			$diff = Diff::compareFiles($base, $tailored);
			$tables_html .= Diff::toTable($diff, '', "", $base_header_text, $tailored_header_text);
			unset($diff);
			$diff = null;

			if (file_exists($new))
			{		
				$diff = Diff::compareFiles($new, $tailored);
				$tables_html .= Diff::toTable($diff, '', "", $new_header_text, $tailored_header_text);
				unset($diff);
				$diff = null;
			}
			else
			{
				$messages .= "No patch file..<br>";
			}

			if (file_exists($generated))
			{
				$diff = Diff::compareFiles($new, $generated);
				$tables_html .= Diff::toTable($diff, '', "", $new_header_text, $generated_header_text);
				unset($diff);
				$diff = null;
			}
			else
			{
				$messages .= "No generated file<br>";
			}

			if (file_exists($upgraded))
			{
				$diff = Diff::compareFiles($tailored, $upgraded);
				$tables_html .= Diff::toTable($diff, '', "", $tailored_header_text, $upgraded_header_text);
				unset($diff);
				$diff = null;
			}
			else
			{
				$messages .= "No upgraded (committed) file<br>";
			}
		}
		else
		{
			$messages .= "<br> Can not make the diff.. No base and/or tailored files..";
		}

		$data = array();
		$data['message'] = $messages;
		$data['result'] = $tables_html;
		$data['success'] = true;
		echo json_encode($data);
	}

	public function CommitDiff($post)
	{
		$success = false;
		$data = array();
	
		$upgrade_cache_new_file = $post['upgrade_cache_new_file'];
		$plain_filename = $post['plain_filename'];
		$upgrade_file = $post['upgraded_filename'];

		$shop = $post['current_shop'];
		
		$file_contents = file_get_contents($upgrade_cache_new_file);
		if (($file_contents) === false)
		{
			throw new Exception(__CLASS__."::CommitDiff() Could not read file contents: $upgrade_cache_new_file");
		}
		$file_contents = Stuff::RemoveTmpVariables($file_contents);
		
		if ((file_put_contents($upgrade_file, $file_contents) !== false))
		{
			$success = true;
		}

		$data['new_diff_row_html'] = Stuff::GetNewDiffRow($shop, $plain_filename, $upgrade_cache_new_file);
		$data['message'] = ($success) ? "Wrote $upgrade_cache_new_file to $upgrade_file" : "Failed to write $upgrade_file";
		$data['success'] = $success;
		echo json_encode($data);
	}
	public function RevertDiff($post)
	{
		$success = false;
		$data = array();

		$shop = $post['current_shop'];
		$upgraded_filename = $post['upgraded_filename'];
		$plain_filename = $post['plain_filename'];
		$upgrade_cache_new_file = $post['upgrade_cache_new_file'];
		if (file_exists($upgraded_filename))
		{
			$success = unlink($upgraded_filename);
			if ($success)
			{
				$data['new_diff_row_html'] = Stuff::GetNewDiffRow($shop, $plain_filename, $upgrade_cache_new_file);
			}
		}

		$data['message'] = ($success) ? "Removed $upgraded_filename" : "Reverting changes failed";
		$data['success'] = $success;
		echo json_encode($data);
	}
	public function FetchTailoredFile($post)
	{
		$plain_filename = $post['plain_filename'];
		$tailored_filename = $post['tailored_filename'];
		$current_shop = $post['current_shop'];
		$success = false;

		$src_public_html = Config::$shops[$current_shop]['public_html_folder'];
		$src_filename = "$src_public_html/$plain_filename";
		$src_filename = str_replace("\\", "/", $src_filename);
		$upgrade_cache_new_file = $post['upgrade_cache_new_file'];
	
		if (strpos($src_filename, "admin/") !== false && isset(Config::$shops[$current_shop]['prospercart_admin_folder']))
		{
			$src_filename = str_replace("admin/", Config::$shops[$current_shop]['prospercart_admin_folder']."/", $src_filename);
		}
		
		try
		{
			$scp = Transfer::GetInstance($current_shop);
			$success = true;
		}
		catch (Exception $e)
		{
			$message =  $e->getMessage();
			$success = false;
		}
		if ($success)
		{
			$file_content = $scp->recv($src_filename, false);
			
			if (!$file_content)
			{
				$message = "SCP::Failed to fetch file $src_filename to $tailored_filename";
				$success = false;	
			}
			else
			{
				if (file_put_contents($tailored_filename, $file_content) !== false)
				{	
					$message = "File saved to $tailored_filename";
					$success = true;
				}
				else
				{
					$message = "File $tailored_filename received but saving failed";
					$success = false;
				}
			}
		}
		else
		{
			$message .= "<br>Tried to fetch $src_filename";
		}

		if ($success)
		{
			$data['new_diff_row_html'] = Stuff::GetNewDiffRow($current_shop, $plain_filename, $upgrade_cache_new_file);
		}
		$data['message'] = $message; 
		$data['success'] = $success;
		echo json_encode($data);
	}
	public function CopyTailoredFileToPatch($post)
	{
		$data = array();
		$success = false;

		$tailored_filename = $post['tailored_filename'];
		$upgrade_file = $post['upgraded_filename'];
		$shop = $post['current_shop'];
		$plain_filename = $post['plain_filename'];
		$upgrade_cache_new_file = $post['upgrade_cache_new_file'];

		if (file_exists($tailored_filename))
		{
			$success = Stuff::CopyFile($tailored_filename, $upgrade_file);
			if ($success)
			{
				$data['new_diff_row_html'] = Stuff::GetNewDiffRow($shop, $plain_filename, $upgrade_cache_new_file);
			}
		}
		
		$data['success'] = $success;
		$data['message'] = $success ? "File copied" : "Failed to copy $tailored_filename to $upgrade_file";
		echo json_encode($data);
	}

	public function CopyNewFileToPatch($post)
	{
		$data = array();
		$success = false;

		$new_filename = $post['new_filename'];
		$upgrade_file = $post['upgraded_filename'];
		$shop = $post['current_shop'];
		$plain_filename = $post['plain_filename'];
		$upgrade_cache_new_file = $post['upgrade_cache_new_file'];

		if (file_exists($new_filename))
		{
			$success = Stuff::CopyFile($new_filename, $upgrade_file);
			if ($success)
			{
				$data['new_diff_row_html'] = Stuff::GetNewDiffRow($shop, $plain_filename, $upgrade_cache_new_file);
			}
		}
		
		$data['success'] = $success;
		$data['message'] = $success ? "File copied" : "Failed to copy $new_filename to $upgrade_file";
		echo json_encode($data);
	}
	public function RemoveTailoredFileFromPatch($post)
	{
		$success = false;
		$data = array();

		$shop = $post['current_shop'];
		$upgraded_filename = $post['upgraded_filename'];
		$plain_filename = $post['plain_filename'];
		$upgrade_cache_new_file = $post['upgrade_cache_new_file'];
		if (file_exists($upgraded_filename))
		{
			$success = unlink($upgraded_filename);
			if ($success)
			{
				$data['new_diff_row_html'] = Stuff::GetNewDiffRow($shop, $plain_filename, $upgrade_cache_new_file);
			}
		}

		$data['success'] = $success;
		$data['message'] = $success ? "File removed" : "Failed to remove $new_filename";
		echo json_encode($data);
	}
	public function PushCommittedDiff($post)
	{
		$data = array();
		$success = false;
		
		$upgraded_filename = $post['upgraded_filename'];
		$plain_filename = $post['plain_filename'];
		$current_shop = $post['current_shop'];
		
		$destination_public_html = Config::$shops[$current_shop]['public_html_folder'];
		$destination_filename = "$destination_public_html/$plain_filename";
		$destination_filename = str_replace("\\", "/", $destination_filename);

		if (strpos($destination_filename, "admin/") !== false && isset(Config::$shops[$current_shop]['prospercart_admin_folder']))
		{
			$destination_filename = str_replace("admin/", Config::$shops[$current_shop]['prospercart_admin_folder']."/", $destination_filename);
		}

		try
		{
			$scp = Transfer::GetInstance($current_shop);
			$success = true;
		}
		catch (Exception $e)
		{
			$message =  $e->getMessage();
			$success = false;
		}
		if ($success)
		{
			if (!$scp->send($upgraded_filename, $destination_filename))
			{
				$message = "SCP::Failed to send file $upgraded_filename to $destination_filename";
				$success = false;	
			}
			else
			{
				$message = "File saved to $destination_filename";
				$success = true;
			}
		}

		$data['message'] = $message; 
		$data['success'] = $success;
		echo json_encode($data);
	}
	public function PushShopOriginal($post)
	{
		$data = array();
		$success = false;
		
		$tailored_filename = $post['tailored_filename'];
		$plain_filename = $post['plain_filename'];
		$current_shop = $post['current_shop'];
		
		$destination_public_html = Config::$shops[$current_shop]['public_html_folder'];
		$destination_filename = "$destination_public_html/$plain_filename";
		$destination_filename = str_replace("\\", "/", $destination_filename);

		if (strpos($destination_filename, "admin/") !== false && isset(Config::$shops[$current_shop]['prospercart_admin_folder']))
		{
			$destination_filename = str_replace("admin/", Config::$shops[$current_shop]['prospercart_admin_folder']."/", $destination_filename);
		}

		try
		{
			$scp = Transfer::GetInstance($current_shop);
			$success = true;
		}
		catch (Exception $e)
		{
			$message =  $e->getMessage();
			$success = false;
		}
		if ($success)
		{
			if (!$scp->send($tailored_filename, $destination_filename))
			{
				$message = "SCP::Failed to send file $upgraded_filename to $destination_filename";
				$success = false;	
			}
			else
			{
				$message = "File saved to $destination_filename";
				$success = true;
			}
		}

		$data['message'] = $message; 
		$data['success'] = $success;
		echo json_encode($data);
	}
	public function PushAllCommittedDiffs($post)
	{
		$local_new_folder = $post['new_folder'];
		$plain_filename = $post['plain_filename'];

		$foldername = $post['real_foldername'];
		$destination_public_html = Config::$connection_params['public_html_folder'];
		$destination_folder = "$destination_public_html/$foldername/";
		
		$scp = Transfer::GetInstance();

		$directory = new RecursiveDirectoryIterator($new);
		$filter = new UpgradeFileFilter($directory, Config::$upgrade["recursive_diff"], Config::$upgrade["filename_override"]);
		$src_files = new RecursiveIteratorIterator($filter, RecursiveIteratorIterator::SELF_FIRST);	
		foreach ($src_files as $fileinfo) 
		{
			$filename = $fileinfo->getPathname();
			$local_new_filename = $new."/".$filename;
			$result .= "$local_new_filename to $destination_folder\n";
			if ($fileinfo->isDir())
			{
				//$scp->mkdir($local_new_filename, $destination_folder);
			}
			else
			{
				/*
				if (!$scp->send($local_new_filename, $destination_folder))
				{
					echo "SCP::Failed to send file $local_new_filename to $destination_folder";
					exit;
				}*/
			}
		}
		$result .= "Files sent";
		return $result;
	}
}