<?php
class UpgradeFileFilter extends RecursiveFilterIterator 
{
	// Filetypes in regex-format
	private $filetypes = "(php|js|css)";
	private $recursive_diff;
	private $filename_override;

	/*
     * A DirectoryIterator filter class constructor
     * @param RecursiveIterator $i 
     * @param boolean $recursive_diff If sub-directories should be accepted
     * @param array $filename_override A optional list of filenames. If applied, only these files will be accepted
	 */
	function __construct(RecursiveIterator $i, $recursive_diff = true, $filename_override = array())
    {
		$this->recursive_diff = $recursive_diff;
		$this->filename_override = $filename_override;
        parent::__construct($i);
    }
	public function accept()
	{
		$filename = $this->current()->getFilename();
		
		// Skip hidden files and directories.
	    if ($filename[0] === '.') 
	    {
	    	return false;
	    }

	    if ($this->isDir())
	    {
	    	return ($this->recursive_diff);
	    }
	    else
	    {
	    	if (!empty($this->filename_override))
	    	{
	    		return (in_array($filename, $this->filename_override));
	    	}
	    	else
	    	{
	    		return (preg_match('/^.+\.'.$this->filetypes.'$/i', $filename) === 1);
	    	}
	    }
	}
}
?>