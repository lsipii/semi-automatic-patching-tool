<?php

class AjaxHandler
{
	public $caller = null;
	public $action = null;
	
	public function __construct()
	{
		if(isset($_POST['action']) && isset($_POST['caller']))
		{
			$result = array("message" => "Unkown error", "success" => false);

			$action = $this->action = $_POST['action'];
			$_POST['action'] = null;
			unset($_POST['action']);

			$caller = $this->caller = $_POST['caller'];
			$_POST['caller'] = null;
			unset($_POST['caller']);

			if(method_exists($this->caller, $this->action))
			{
				try 
				{
					ob_start();
					$caller::$action($_POST);	
					$result = ob_get_clean();
				} 
				catch (Exception $e) 
				{
					$result["message"] = $caller."::".$e->getMessage();
				}
			}

			if (is_array($result))
			{
				$result = json_encode($result);
			}
			echo $result;
			exit;
		}
	}
}