<?php
$phpseclib_path = realpath("core/libs/ssh/phpseclib/");
$ssh_include_path = realpath("core/libs/ssh/");
set_include_path(get_include_path().PATH_SEPARATOR.$ssh_include_path.PATH_SEPARATOR.$phpseclib_path);
include('phpseclib.php');
require('class.SSH2Authentication.php');
require('class.SSH2Password.php');
require('class.SSH2.php');
require('class.SSH2SCP.php');

class Transfer extends SSH2SCP
{
	private static $instance;
	private static $current_shop = "";
	public static function getInstance($current_shop)
	{
		if (!isset(self::$instance) || (self::$current_shop != $current_shop))
		{
			self::$instance = new self($current_shop);	
		}
		return self::$instance;
	}
	public static $connection_params = array();
	public function __construct($current_shop)
	{
		try
		{
			if (isset(Config::$shops[$current_shop]['username']) && isset(Config::$shops[$current_shop]['password']))
			{
				parent::__construct(Config::$shops[$current_shop]['hostname'], new SSH2Password(Config::$shops[$current_shop]['username'], Config::$shops[$current_shop]['password']));
			}
			else if (isset(Config::$shops[$current_shop]['username']) && isset(Config::$shops[$current_shop]['public_key']) && isset(Config::$shops[$current_shop]['private_key']))
			{
				parent::__construct(Config::$shops[$current_shop]['hostname'], new SSH2Key(Config::$shops[$current_shop]['username'], Config::$shops[$current_shop]['public_key'], Config::$shops[$current_shop]['private_key']));
			}
		}
		catch (Exception $e)
		{
			throw new Exception("SCP::Fatal exception: ". $e->getMessage());
		}
	}
}
?>