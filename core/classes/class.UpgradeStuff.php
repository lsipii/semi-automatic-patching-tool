<?php
require_once('class.Stuff.php');
class UpgradeStuff extends Stuff
{
	function __construct($current_shop, $filename_called_from_ajax = null)
	{
		foreach (Config::$upgrade as $variable => $value)
		{
			if (property_exists(__CLASS__, $variable))
			{
				$this->$variable = $value;					
			}
		}

		$this->current_working_directory = Config::$upgrade['working_directory'].VQMod::$directorySeparator.$current_shop.VQMod::$directorySeparator; 
		parent::__construct($current_shop);
		$this->initStuff(__CLASS__, $filename_called_from_ajax);


		if (!$filename_called_from_ajax)
		{
			// If not exists, create working directory structure from template
			$working_directory_template = "core".VQMod::$directorySeparator."templates".VQMod::$directorySeparator."src".VQMod::$directorySeparator;
			$this->CopyStuffToStuff($working_directory_template, $this->current_working_directory);

			if (Config::$general_operation_info["upgrade_or_patch"] == "patch")
			{
				// DEBUG: Refresh the new version folder :: Make a button 
				//$this->DeleteStuffFromStuff($this->new_version_folder);

				$patch_folder = Config::$patches["working_directory"].VQMod::$directorySeparator.$this->shop_version.VQMod::$directorySeparator.Config::$general_operation_info["upgrade_folder"].VQMod::$directorySeparator;
				$this->CopyStuffToStuff($patch_folder, $this->new_version_folder);			
			}
		}
	}
	public function overrideDiffContainerTemplate($template_data = array())
	{
		$template_data["caller"] = __CLASS__; // For AjaxHandler

		if (empty($template_data["errors"]))
		{
			if (file_exists($template_data["upgraded_filename"]))
			{
				$template_data["file_message"] = "Committed file:";
				$template_data["file_controls"] =" ----- <a href='' class='file_control RevertDiff'>Revert the commit</a>"; 	
				$template_data["file_controls"] .=" ----- <a href='' class='file_control PushCommittedDiff'>Push the committed file to the server.</a>"; 	
				$template_data["file_controls"] .=" ----- <a href='' class='file_control PushShopOriginal'>Push the original file to the server.</a>"; 	
			}
			else if (file_exists($template_data["upgrade_cache_new_file"]))
			{
				$template_data["file_message"] = "VQMod: show generated file:";
				$template_data["file_controls"] =", save generated changes to the upgrade folder: <a href='' class='file_control CommitDiff'>Commit</a>"; 	
			}
			else
			{
				$template_data["file_message"] = "VQMod: ".$template_data["new_filename"]." not generated..:";
			}
		}
		parent::printDiffContainerTemplate($template_data);
	}

	public function CheckIfFileExistsOnServer($post)
	{
		$data = array();
		$success = false;

		ob_start();
		self::FetchTailoredFile($post);
		$fetch_data = ob_get_clean();
		$fetch_data = json_decode($fetch_data, true);
		if ($fetch_data['success'])
		{
			$fetch_data['message'] = "The file was copied from server";
			$data = $fetch_data;
		}
		else
		{
			$last_fetch_message = $fetch_data['message'];
			ob_start();
			self::CopyNewFileToPatch($post);
			$fetch_data = ob_get_clean();
			$fetch_data = json_decode($fetch_data, true);
			$fetch_data['message'] = $last_fetch_message."<br>";
			$fetch_data['message'] .= "The file was not on server, copied from the patch.";
			$data = $fetch_data;
		}
		echo json_encode($data);
	}
	public function ClearGenerations($params)
	{

		// Truncate the shops working directory
		$current_working_directory = Config::$upgrade['working_directory'].VQMod::$directorySeparator.$params['version'].VQMod::$directorySeparator;
		$success = Stuff::DeleteStuffFromStuff($current_working_directory);
		
		$data = array();
		$data['success'] = $success;
		$data['message'] = ($success) ? "" : "Erasing $current_working_directory just somehow failed";
		echo json_encode($data);
	}
}
?>