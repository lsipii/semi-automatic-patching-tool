<?php

// include the Diff class
require_once('class.Diff.php');

// include vqmod
require_once("vqmod.php");
VQMod::EnsureDirectorySeparator();

// require the vqmod xml creator
require_once('class.VQModXMLGenerator.php');
require_once('class.UpgradeFileFilter.php');
require_once('class.AjaxCalls.php');

class Stuff extends AjaxCalls
{
	public $current_shop = null;
	public $current_working_directory = "";
	public $version_control_folders = array();
	public $working_directory = "";
	public $base_versions_directory = null;
	public $base_filepath_prefix = null;
	private $caller_type = "";
	public $initial_patch_call = false;

	public $new_version_folder = null;
	public $base_version_folder = null;
	public $upgrade_folder = null;
	public $tailored_version_folder = null;

	public $vqcreator = null;

	public $shop_version = null;
	public $new_version = null;

	public $recursive_diff;
	public $filename_override = false;
	public $modified_files = array();
	public $non_conflicting_files = array();
	public $not_found_in_destination = array();	
	public $not_found_in_src = array();

	public static $modified_line_prefix = "SUPERDUPER_PREFIX_REMOVE_THIS_IMMEDIATELY";
	public static $modified_line_postfix = "SUPERDUPER_POSTFIX_REMOVE_THIS_IMMEDIATELY";	
	public static $tailored_lines_start = "SUPERDUPER_TAILORED_START_REMOVE_THIS_IMMEDIATELY";
	public static $tailored_lines_end = "SUPERDUPER_TAILORED_END_REMOVE_THIS_IMMEDIATELY";	

	function __construct($current_shop = null)
	{
		if (!$current_shop || !$this->current_working_directory)
		{
			throw new Exception(__CLASS__.": Shop folder or working directory undefined..");	
		}
		$this->current_shop = $current_shop;

		Config::$general_operation_info["new_version"] = (Config::$general_operation_info["upgrade_or_patch"] == "patch") ? Config::$shops[$this->current_shop]["shop_version"].Config::$general_operation_info["upgrade_to_version"] : Config::$general_operation_info["upgrade_to_version"];

		$this->modified_files = array();

		foreach (Config::$general_operation_info as $variable => $value)
		{
			if (property_exists(__CLASS__, $variable))
			{
				$this->$variable = $value;					
			}
		}

		foreach (Config::$shops[$this->current_shop] as $variable => $value)
		{
			if (property_exists(__CLASS__, $variable))
			{
				$this->$variable = $value;					
			}
		}
		if (isset($this->base_versions_directory))
		{
			$mainVersionNumber = substr($this->shop_version,0,3);
			$this->base_version_folder = $this->base_versions_directory.VQMod::$directorySeparator.$mainVersionNumber.VQMod::$directorySeparator.$this->shop_version.VQMod::$directorySeparator;
			if (isset($this->base_filepath_prefix))
            {
                $this->base_version_folder .= $this->base_filepath_prefix.VQMod::$directorySeparator;
            }

		}
		if (!$this->base_version_folder || !$this->tailored_version_folder || !$this->new_version_folder || !$this->upgrade_folder)
		{
			throw new Exception(__CLASS__.": Some required constructor params are undefined..");	
		}
		if (!file_exists($this->base_version_folder))
		{
			throw new Exception(__CLASS__.": Base folder for $this->shop_version not created! -- $this->base_version_folder");
		}

		$this->modified_files = array();
	}
	public function initStuff($type = "", $filename_called_from_ajax = null)
	{
		$this->caller_type = $type;
		// Composite VQModXMLGenerator
		$vq_params = array_merge(
			Config::$general_operation_info,  
			array("current_working_directory" => $this->current_working_directory,
				 "new_version_folder" => $this->new_version_folder,
				 "shop_version" => Config::$shops[$this->current_shop]["shop_version"],
				 "type" => $type
				));
		$this->vqcreator = new VQModXMLGenerator($vq_params, $filename_called_from_ajax);
		$vq_params = null;
		unset($vq_params);

		$this->tailored_version_folder = $this->current_working_directory.$this->tailored_version_folder.VQMod::$directorySeparator;
		$this->new_version_folder = $this->current_working_directory.$this->new_version_folder.VQMod::$directorySeparator;
		$this->upgrade_folder = $this->current_working_directory.$this->upgrade_folder.VQMod::$directorySeparator;

		// Make sure we have a working directory
		$this->EnsureDirs($this->current_working_directory);
	}

	function __destruct()
	{
		$this->vqcreator = null;
		$this->modified_files = null;
		unset($this->vqcreator);
	}

	public function CreateStuff()
	{
		try 
		{
			if (!$this->version_control_folders)
			{
				$this->TraverseFolder($this->new_version_folder);
			}
			else
			{
				foreach ($this->version_control_folders as $folder)
				{
					// Path in the current patching workspace
					$real_folder = $this->new_version_folder.$folder;
					$this->TraverseFolder($real_folder);		
				}
			}

			// Finally check the mods
			$this->vqcreator->CheckMods(); // Throws
		} 
		catch (Exception $e) 
		{
			throw new Exception(__CLASS__."::".$e->getMessage());			
		}
	}
	public function TraverseFolder($folder, $callback = "HandleFileinfo")
	{
		if (method_exists($this, $callback))
		{
			$directory = new RecursiveDirectoryIterator($folder);
			$filter = new UpgradeFileFilter($directory, $this->recursive_diff, $this->filename_override);
			$src_files = new RecursiveIteratorIterator($filter, RecursiveIteratorIterator::SELF_FIRST);	
			
			foreach ($src_files as $fileinfo)
			{		
				$this->$callback($fileinfo);	
			}
		}
		else
		{
			throw new Exception(__CLASS__."::TraverseFolder: '$callback' Not a defined callback.");	
		}
	}
	public function HandleFileinfo($fileinfo)
	{
		$filepath_new = $fileinfo->getPathname(); // Patched file
		$filepath_parent_folder = $fileinfo->getPath();

		$filepath_base = str_replace($this->new_version_folder, $this->base_version_folder, $filepath_new);
		$filepath_tailorings = str_replace($this->new_version_folder, $this->tailored_version_folder, $filepath_new);
		$filepath_tailorings_folder = str_replace($this->new_version_folder, $this->tailored_version_folder, $filepath_new);
		$filepath_tailorings_parent_folder = str_replace($this->new_version_folder, $this->tailored_version_folder, $filepath_parent_folder);
		
		$filepath_upgrade_folder = str_replace($this->new_version_folder, $this->upgrade_folder, $filepath_new);
		$filepath_upgrade_parent_folder = str_replace($this->new_version_folder, $this->upgrade_folder, $filepath_parent_folder);
		if ($filepath_upgrade_parent_folder == $filepath_parent_folder)
		{
			$filepath_upgrade_parent_folder = "./";
		}

		$clear_filepath = str_replace($this->new_version_folder, "", $filepath_new);
		$clear_filepath_parent_folder = str_replace($this->new_version_folder, "", $filepath_parent_folder);

		/*echo $clear_filepath."<br>";
		echo $filepath_base.file_exists($filepath_base)."<br>";
		echo $filepath_tailorings.file_exists($filepath_tailorings)."<br>";
		echo $filepath_new.file_exists($filepath_new)."<br>";
		echo $filepath_upgrade_folder.file_exists($filepath_upgrade_folder)."<br>";*/
		

		// This could happen when call came from ajax
		if (!file_exists($filepath_new))
		{
			return;
		}

		// Skip ignored filepaths
		if (isset(Config::$general_operation_info["skip_these_files"]))
		{
			if (in_array($clear_filepath, Config::$general_operation_info["skip_these_files"]))
			{
				return;
			}
		}
		
		// Skip filepaths not in override list	
		if (isset($this->filename_override) && !empty($this->filename_override))
		{
			
			if (!in_array($clear_filepath, $this->filename_override))
			{
				return;
			}
		}


		if ($fileinfo->isDir())
		{
			$this->EnsureDirs($filepath_upgrade_folder);
			$this->EnsureDirs($filepath_tailorings_folder);
		}
		else 
		{
			$vqcache_file = $this->current_working_directory.VQMod::GetCachenameFromFileame($clear_filepath);
			// Skip already generated files
			if (file_exists($vqcache_file))
			{
				$this->modified_files[$clear_filepath] = $clear_filepath;
				return;
			}
			$this->EnsureDirs($filepath_upgrade_parent_folder);
			$this->EnsureDirs($filepath_tailorings_parent_folder);
	
			if (file_exists($filepath_tailorings) && file_exists($filepath_base))
			{

				if ($this->initial_patch_call)
				{	

					// Ensure that the initial patch files really are different
					$filepath_base_file_contents = file_get_contents($filepath_base);
					$filepath_base_file_contents = nl2br($filepath_base_file_contents);

					$filepath_tailorings_contents = file_get_contents($filepath_tailorings);
					$filepath_tailorings_contents = nl2br($filepath_tailorings_contents);

					if (mb_strlen($filepath_base_file_contents) === mb_strlen($filepath_tailorings_contents))
					{
						return;
					}	
				}

				$this->vqcreator->setFilename($clear_filepath);
				$file_not_modified = true;

				// compare tailored files to the base version files line by line
				$diff = Diff::compareFiles($filepath_base, $filepath_tailorings);
				
				// Logic from Diff::toTable()		
				$index = 0;
				while ($index < count($diff))
				{
					$last_unmodifieds = array();
					$last_deletes = array();
					$last_inserts = array();
					$modified = false;

					switch ($diff[$index][1]) 
					{
						case Diff::UNMODIFIED:
							$last_unmodifieds = $this->getDiffLinesForType($diff, $index, Diff::UNMODIFIED);
							break;
						case Diff::DELETED:
							$modified = true;
							$last_deletes = $this->getDiffLinesForType($diff, $index, Diff::DELETED);
							$last_inserts = $this->getDiffLinesForType($diff, $index, Diff::INSERTED);
							break;
						case Diff::INSERTED:
							$modified = true;
							$last_inserts = $this->getDiffLinesForType($diff, $index, Diff::INSERTED);
							break;
					}
					if ($modified == true)
					{
						$file_not_modified = false;
						$this->modified_files[$clear_filepath] = $clear_filepath;
					}

					$this->vqcreator->AppendRules($modified, $last_unmodifieds, $last_deletes, $last_inserts);
					$last_unmodifieds = $last_deletes = $last_inserts = null;
					unset($last_unmodifieds, $last_deletes, $last_inserts);
				}

				if ($file_not_modified)
				{
					// Copy non-conflicting file to the vqmod cache folder
					$this->CopyFile($filepath_new, $vqcache_file);
					$this->non_conflicting_files[$clear_filepath] = $clear_filepath;
				}

				$diff = null;
				unset($diff);
				$this->vqcreator->Commit();	// Throws
			}
			else
			{
				//throw new Exception("HandleFileinfo: Tailored file $filepath_tailorings could not be found!");
				if (file_exists($filepath_base))
				{
					$this->not_found_in_destination[] = $clear_filepath;
				}
				else
				{
					$this->not_found_in_src[] = $clear_filepath;
				}
				return; 
			}
		}
	}
	/*
	 * Logic from Diff::getDiffLinesForType()
	 */
	public function getDiffLinesForType($diff, &$index, $type)
	{
		$changes = array();
		while ($index < count($diff) && $diff[$index][1] == $type)
		{
			$change = $diff[$index][0];
			$changes[] = $change;	  
			$index++;
		}
		return $changes;
	}

	public function CheckSingleFileAndCreateStuff($clear_filepath)
	{
		$fileinfo = new SplFileObject($this->new_version_folder.$clear_filepath); // Throws
		$this->HandleFileinfo($fileinfo); // Throws
		$this->vqcreator->CheckMods(); // Throws
	}	
	// Does what the function name says
	public function DiDeDiffTadleDonDerrorDilesDplease()
	{	
		
		// Files that was not found from tailored working directory
		if (!empty($this->not_found_in_destination))
		{
			foreach ($this->not_found_in_destination as $key => $filename) 
			{
				$template_data = $this->CreateDefaultTemplateArray($filename);
				$fetch_from_where = ($this->caller_type == "GenerateStuff") ? "Generate from the initial patch" : "Fetch from the server";
				$template_data["file_message"] = "Not found:";
				$template_data["file_controls"] .=" ----- <a href='' class='file_control FetchTailoredFile'>$fetch_from_where</a>"; 
				$template_data["errors"] = " ";
				$this->overrideDiffContainerTemplate($template_data);
			}
		}

		// Files that were modified
		if (!empty($this->modified_files))
		{
			$vqmod_error_files = array_keys($this->vqcreator->vqmod_errors);
			foreach ($this->modified_files as $key => $filename) 
			{	
				$template_data = $this->CreateDefaultTemplateArray($filename);
				
				if (in_array($filename, $vqmod_error_files))
				{
					$template_data["file_message"] = "VQMod error on file:";		
					$template_data["errors"] = "<br>Errors: <br>"; 
					$template_data["errors"] .= "$template_data[tailored_filename] ==> $template_data[new_filename] ==> $template_data[upgrade_cache_new_file]<br>";
					foreach ($this->vqcreator->vqmod_errors[$filename] as $key => $value)
					{
						$template_data["errors"] .= "<pre>".$value["error_msg"].": \"".htmlspecialchars($value['search'])."\"";
						$template_data["errors"] .= ", position: $value[position], index: $value[index], offset: $value[offset]";
						
						$template_data["errors"] .= "<br>vqmod replacement: \"".self::RemoveTmpVariables(htmlspecialchars($value['add']))."\"";
						$template_data["errors"] .= "</pre><br>";
					}
				}

				$this->overrideDiffContainerTemplate($template_data);	
			}
		}

		// Files that are considered as a new files coming from the patch
		if (!empty($this->not_found_in_src))
		{
			foreach ($this->not_found_in_src as $key => $filename) 
			{
				$template_data = $this->CreateDefaultTemplateArray($filename);
				$fetch_from_where = ($this->caller_type == "GenerateStuff") ? "Copy from the initial patch" : "If not exists on server, copy from initial patch";
				$template_data["file_message"] = "This is a new file :";
				if (file_exists($this->upgrade_folder."/".$filename))
				{
					$template_data["file_controls"] .=" ----- <a href='' class='file_control RemoveTailoredFileFromPatch'>Remove from upgraded folder</a>"; 
					if ($this->caller_type == "UpgradeStuff")
					{ 
						$template_data["file_controls"] .=" ----- <a href='' class='file_control PushCommittedDiff'>Push the file to the server.</a>"; 
					}
				}
				else
				{
					$init_action = ($this->caller_type == "GenerateStuff") ? "CopyTailoredFileToPatch" : "CheckIfFileExistsOnServer";
					$template_data["file_controls"] .=" ----- <a href='' class='file_control $init_action'>$fetch_from_where</a>"; 
				}
				$template_data["errors"] = "";
				$this->overrideDiffContainerTemplate($template_data);
			}
		}

		// Files that alerted no conflicts
		if ($this->caller_type != "GenerateStuff")
		{
			if (!empty($this->non_conflicting_files))
			{
				foreach ($this->non_conflicting_files as $key => $filename) 
				{
					$template_data = $this->CreateDefaultTemplateArray($filename);
					$template_data["file_message"] = "No modifications.. ";
					$template_data["file_controls"] .=" ----- <a href='' class='file_control CopyNewFileToPatch'>Copy to upgrade folder</a>"; 
					$template_data["errors"] = "";
					$this->overrideDiffContainerTemplate($template_data);
				}
			}
		}
	}
	private function CreateDefaultTemplateArray($filename)
	{
		$template_data = array();
		$template_data["caller"] = __CLASS__;
		$template_data["current_shop"] = $this->current_shop;
		$template_data["file_controls"] = "";
		$template_data["errors"] = "";

		$template_data["upgrade_cache_new_file"] = $this->current_working_directory.VQMod::GetCachenameFromFileame($filename);
		$template_data["upgrade_cache_new_file_header_text"] = "Generated $this->new_version";
						
		$template_data["new_filename"] = $this->new_version_folder.$filename;
		$template_data["base_filename"] = $this->base_version_folder.$filename;
		$template_data["upgraded_filename"] = $this->upgrade_folder.$filename;
		$template_data["tailored_filename"] = $this->tailored_version_folder.$filename;
		$template_data["plain_filename"] = $filename;
		$template_data["plain_folder"] = dirname($filename);
		
		$container_id = $this->current_shop."-".$template_data["plain_folder"]."-".$template_data["plain_filename"];
		$container_id = str_replace(".", "-", $container_id);
		$container_id = str_replace("/", "-", $container_id);
		$container_id = str_replace("\\", "-", $container_id);		
		$template_data["diff_container_id"] = $container_id;

		$template_data["base_version_file_header_text"] = "Base $this->shop_version";
		$template_data["new_version_file_header_text"] = "Patched $this->new_version";
		$template_data["tailored_version_file_header_text"] = "Tailored $this->shop_version";
		$template_data["upgraded_version_file_header_text"] = "Upgraded $this->new_version";
		$template_data["fileByteChanges"] = $this->fileBytesHtml($template_data);
		
		return $template_data;
	}
	public function overrideDiffContainerTemplate($template_data = array())
	{
		self::printDiffContainerTemplate($template_data);
	}
	public static function printDiffContainerTemplate($template_data = array())
	{
		$template_filename = "core".VQMod::$directorySeparator."templates".VQMod::$directorySeparator."template.diff_container.html";
		$template = file_get_contents($template_filename);
		foreach ($template_data as $template_var => $value) 
		{
			$template = str_replace('{'.$template_var.'}', $value, $template);
		}
		echo $template;
	}	
	public function EnsureDirs($folder)
	{
		if (!file_exists($folder))
		{
			if (mkdir($folder, 0777, true) === FALSE)
			{
				echo "Failed to create directory '$folder'..<br>";
				exit;
			}
		}
	}
	public static function CopyFile($src_filename, $dest_filename)
	{
		if (file_exists($src_filename) && !file_exists($dest_filename))
		{
			return copy($src_filename, $dest_filename);
		}
		return;
	}
	public function CopyStuffToStuff($source_folder, $destination_folder, $dest_filename_overwrites = array(), $replace_content_with_overwrites = false)
	{
		$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source_folder, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST);
		foreach ($iterator as $item) 
		{
			if ($item->isDir()) 
			{
				$this->EnsureDirs($destination_folder . VQMod::$directorySeparator . $iterator->getSubPathName());
			} 
			else 
			{
				$filepath =  $destination_folder . VQMod::$directorySeparator . $iterator->getSubPathName();
				
				if (!empty($dest_filename_overwrites))
				{
					$filepath = strtr($filepath, $dest_filename_overwrites);
				}
	
				if ($replace_content_with_overwrites && !empty($dest_filename_overwrites))
				{
					$file_contents = file_get_contents($item);
					if (($file_contents) === false)
					{
						throw new Exception(__CLASS__."::CopyStuffToStuff() Could not read file contents: $item");
					}
					$file_contents = strtr($file_contents, $dest_filename_overwrites);
						
					if ((file_put_contents($filepath, $file_contents) === false))
					{
						throw new Exception(__CLASS__."::CopyStuffToStuff() Could not write file contents: $filepath");
					}
				}
				else if (!file_exists($filepath))
				{
					copy($item, $filepath);
				}

			}
		}
	}
	public function DeleteStuffFromStuff($delete_folder)
	{
		$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($delete_folder, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);
		$success = false;
		foreach ($iterator as $item) 
		{
			if ($item->isDir()) 
			{
				$success = rmdir($iterator->getRealPath());
			} 
			else 
			{
				$success = unlink($iterator->getRealPath());
			}
		}
		return $success;
	}
	public static function RemoveTmpVariables($file_contents) 
	{
		$file_contents = str_replace(self::$modified_line_prefix, "", $file_contents);
		$file_contents = str_replace(self::$modified_line_postfix, "", $file_contents);
		$file_contents = str_replace(self::$tailored_lines_start, "", $file_contents);
		$file_contents = str_replace(self::$tailored_lines_end, "", $file_contents);
		return $file_contents;
	}

	public function GetNewDiffRow($shop, $real_filename, $cache_filename = null)
	{

		// The second param indicates that we're creating the object from ajax call
		$upgrade_stuff = new $this->caller($shop, $cache_filename);
		$upgrade_stuff->CheckSingleFileAndCreateStuff($real_filename);

		//$upgrade_stuff->setModifiedFiles(array($real_filename));
		ob_start();
		$upgrade_stuff->DiDeDiffTadleDonDerrorDilesDplease();
		$new_diff_line = ob_get_clean();
		
		$upgrade_stuff = null;
		unset($upgrade_stuff);
		
		return $new_diff_line;
	}
	public function setModifiedFiles($files = array())
	{
		$this->modified_files = $files;
	}
	private function fileBytesHtml($template_data)
	{
		$byteData = array();
		$html = "";

		if(isset($template_data["new_filename"]) && file_exists($template_data["new_filename"]))
		{	
			$byteData["new_filename"] = filesize($template_data["new_filename"]); 
		}
		if(isset($template_data["base_filename"]) && file_exists($template_data["base_filename"]))
		{	
			$byteData["base_filename"] = filesize($template_data["base_filename"]); 
		}
		if(isset($template_data["tailored_filename"]) && file_exists($template_data["tailored_filename"]))
		{	
			$byteData["tailored_filename"] = filesize($template_data["tailored_filename"]); 
		}
		if(isset($template_data["upgraded_filename"]) && file_exists($template_data["upgraded_filename"]))
		{	
			$byteData["upgraded_filename"] = filesize($template_data["upgraded_filename"]); 
		}
		if (!empty($byteData))
		{
			$visibilityCssClass = "hidden";
			$lastRow = "";
			if (isset($byteData["upgraded_filename"]) && isset($byteData["tailored_filename"]) && isset($byteData["base_filename"]))
			{
				$newToBaseDifference = $byteData["new_filename"] - $byteData["base_filename"];
				$tailoringToBaseDifference = $byteData["tailored_filename"] - $byteData["base_filename"];
				$changesToBase = $newToBaseDifference + $tailoringToBaseDifference;
				$savedChangesToBase = $byteData["upgraded_filename"] - $byteData["base_filename"];
				
				if ($savedChangesToBase !== $changesToBase)
				{
					$visibilityCssClass = "alert";
					$lastRow = "<tr class='danger'>";
					$lastRow .= "<td>Au! Something be wrong!</td>";
					$lastRow .= "<td>Diff from tailorings and the upgrade to the base: $changesToBase<br>";
					$lastRow .= "Generated/saved diff to the base: $savedChangesToBase</td>";
					$lastRow .= "</td>";
				}

			}
			$html .= "<table class='allTheBytes $visibilityCssClass'>";
			$html .= "<th>Filetype</th><th>Bytes</th>";
			foreach ($byteData as $byteName => $bytes)
			{
				$html .= "<tr>";
				$html .= "<td>$byteName</td> <td>$bytes</td>";
				$html .= "</tr>";
			}
			$html .= $lastRow;
			$html .= "</table>";
		}
		return $html;
	}
}
