$(document).ready( function() {
	$("body").delegate("#initial_view", "click", function(e) {
		e.preventDefault();
		var location = window.location.href.substring(0, window.location.href.lastIndexOf("/"));
		$("#tool_container").load(location + "?view=initial #tools");
	});
	$("body").delegate("#generate_view", "click", function(e) {
		e.preventDefault();
		var location = window.location.href.substring(0, window.location.href.lastIndexOf("/"));
		$("#tool_container").load(location + "?view=generate #tools");
	});
	$("body").delegate("#upgrade_view", "click", function(e) {
		e.preventDefault();
		var location = window.location.href.substring(0, window.location.href.lastIndexOf("/"));
		$("#tool_container").load(location + "?view=upgrade #tools");
	});

	$('body').delegate("#toggle_readme", 'click', function() {
		toggleReadme();
	});
	$('body').keydown(function(e) {
	    if (e.keyCode == "27") // control key "esc"
	    {
	    	toggleReadme();	
	    }
	    if (e.keyCode == "116") // control key "F5"
	    {
	    	e.preventDefault();
	    	var href = window.location.href;
	    	var href_tag = href.split('/').pop();
	    	if (href_tag.length)
	    	{
	    		href = href.replace(href_tag, "");
	    	}
	    	//console.log(href);
	    	window.location.href = href;
	    }
	    if (e.keyCode == "16") // shift
	    {
	    	shift_pressed.ShiftActivated(true);
	    }
	    if (e.keyCode == "32") // space
	    {
	    	e.preventDefault();
	    	var href_tag = window.location.href.split('/').pop();
	    	if (href_tag.length)
	    	{
	    		var tag_number_string = href_tag.replace('#title', '');
	    		var tag_number_int = parseInt(tag_number_string);
	    		var count_possible_tags = $("#toc a").length;
	    		if (shift_pressed.RightKeyCombination(e.keyCode))
	    		{
	    			if (tag_number_int == 1 || tag_number_int == 0)
		    		{
		    			tag_number_int = count_possible_tags;
		    		}	
		    		else
		    		{
		    			tag_number_int -= 1;
		    		}
	    		}
	    		else
	    		{
		    		if (tag_number_int < count_possible_tags)
		    		{
		    			tag_number_int += 1;
		    		}	
		    		else
		    		{
		    			tag_number_int = 1;
		    		}
		    	}
	    		href_tag = "#title"+tag_number_int.toString();
	    	}
	    	else
	    	{
	    		href_tag = "#title1";
	    	}
	    	window.location.href = href_tag;
	    }
	    //console.log(e.keyCode);
	});
	
	var navigointiohje = "<br>Dokumentissa navigointi: <ul><li>Seuraava otsake: välilyönti</li><li>Edellinen otsake: shift+välilyönti</li><li>Takaisin alkuun: home, f5</li><li>Työkaluun ja takaisiin: Esc</li></ul>";

	$("#readme_container h2:first").before("<div id='toc'><h2 class='skip_toc'>Esityksen otsakkeet:</h2></div>"+navigointiohje);
	$("#readme_container h2, #readme_container h3").each(function(i) {
	    var current = $(this);
	   	if (!current.hasClass("skip_toc"))
	   	{
		    current.attr("id", "title" + i);
		    $("#toc").append("<a id='link" + i + "' href='#title" +
		        i + "' title='" + current.attr("tagName") + "' class='toc_"+current.attr('tagName')+"'>" + 
		        current.html() + "</a>");
		}
	});
});
var lastToolPosition = 0;
var lastReadmePosition = 0;
function toggleReadme()
{
	if (!$("#readme_container").hasClass("hidden"))
	{
		lastReadmePosition = $(document).scrollTop();
		$("#readme_container").addClass("hidden");
		$("html, body").scrollTop(lastToolPosition);
	}
	else
	{
		lastToolPosition = $(document).scrollTop();
		$("#readme_container").removeClass("hidden");
		$("html, body").scrollTop(lastReadmePosition);
	}
}
var shift_pressed = new function() {
   this.yes_please = false;
   this.last_push_time = null;

   this.ShiftActivated = function(value)
   {
            this.yes_please = value;
            if (value)
            {
                    this.last_push_time = new Date().getTime();
            }
            else
            {
                    this.last_push_time = null;
            }
   };
   this.RightKeyCombination = function(keyCode)
   {
        this.yes_please = false;

        if (this.last_push_time)
        {
            var just_now = new Date().getTime();
            var difference = just_now - this.last_push_time;
            if (difference < 200)
            {
                    this.yes_please = true;
            }
            this.last_push_time = null;
        }

        if (this.yes_please)
        {
                if (keyCode == "32") // shift + space
                {
                }
                else
                {
                    this.yes_please = false;
                }

                this.last_push_time = null;
        }

        return this.yes_please;
   };
}
