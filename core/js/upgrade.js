$(document).ready( function() {

	$('body').delegate('.diff_container .file_control', 'click',function(e) 
	{
		
		e.preventDefault();
		var trigger_parent = $(this).parent();
		var trigger_parent_id = trigger_parent.attr("id");
		var params = {
			action: $(this).attr("class").replace("file_control", "").trim()
		};

		trigger_parent.find("span.hidden").each(function() {
			var attribute = $(this).attr("class").replace("hidden", "").trim();
			params[attribute] = $(this).text();
		});
		
		$.post(window.location, params, function(data) {
			var message = "Something failed..";
			if (data.new_diff_row_html)
			{
				trigger_parent.after(data.new_diff_row_html).remove();
			}
			if (data.message)
			{
				message = data.message;
			}
			$("#"+trigger_parent_id+" .notice").html(message);	
		}, "json");
	});

	
	$('body').delegate('.diff_container .push_all_commits', 'click',function(e) 
	{
		e.preventDefault();
		
		alert("Not fixed after refactoring..");
		return;
		var trigger = $(this);
		var params = {
			action: "PushAllCommittedDiffs",
			new_folder: trigger.parent().find(".new_folder").text(),
			plain_foldename: trigger.parent().find(".plain_folder").text()
		};
		$.post(window.location, params, function(data) {
			var message = "Something failed..";
			if (data.message)
			{
				message = data.message;
			}
			if (data.new_diff_row_html)
			{
				trigger.parent().after(data.new_diff_row_html).remove();
			}
			$('#notice').html(message);	
		}, "json");
	});
	
	$('body').delegate('.diff_container .toggle', 'click',function(e) 
	{
		e.preventDefault();
		
		var trigger = $(this);
		var trigger_parent_id = trigger.parent().attr("id");
		
		var diff_file = trigger.parent().find(".diff_file");
		if (diff_file.hasClass("hidden"))
		{
			var changed_lines = [];

			diff_file.removeClass("hidden");
			diff_file.addClass("visible");
		
			var params = {
				action: "GetAllTheDiffs",
				caller: trigger.parent().find(".caller").text(),
				current_shop: trigger.parent().find(".current_shop").text(),
				new_filename: trigger.parent().find(".new_filename").text(),
				new_version_file_header_text: trigger.parent().find(".new_version_file_header_text").text(),	
				base_filename: trigger.parent().find(".base_filename").text(),
				base_version_file_header_text: trigger.parent().find(".base_version_file_header_text").text(),
				tailored_filename: trigger.parent().find(".tailored_filename").text(),
				tailored_version_file_header_text: trigger.parent().find(".tailored_version_file_header_text").text(),
				upgraded_filename: trigger.parent().find(".upgraded_filename").text(),
				upgraded_version_file_header_text: trigger.parent().find(".upgraded_version_file_header_text").text(),
				upgrade_cache_new_file: trigger.parent().find(".upgrade_cache_new_file").text(),
				upgrade_cache_new_file_header_text: trigger.parent().find(".upgrade_cache_new_file_header_text").text(),
			};
			$.post(window.location, params, function(data) {
				if (data.message)
				{
					trigger.parent().find(".notice").html(data.message);	
				}
				if (data.success)
				{
					diff_file.html(data.result);

					var go_green = false;
					var go_orange = false;
					
					trigger.parent().find("tr").each(function() {
					
						if ($(this).find("span:contains(SUPERDUPER_PREFIX_REMOVE_THIS_IMMEDIATELY)").length)
						{
							$(this).find("td:nth-child(2) span").each(function() 
							{
								if ($(this).html().indexOf("SUPERDUPER_PREFIX_REMOVE_THIS_IMMEDIATELY") != -1)
								{
									go_green = true;
									$(this).html($(this).html().replace("SUPERDUPER_PREFIX_REMOVE_THIS_IMMEDIATELY", ""));
								}

								if (go_green)
								{
									changed_lines.push($(this).attr("id"));
									$(this).parent().prev().removeClass("diffDeleted");
									$(this).addClass("modified_line");
								}

								if ($(this).html().indexOf("SUPERDUPER_POSTFIX_REMOVE_THIS_IMMEDIATELY") != -1)
								{
									go_green = false;
									$(this).html($(this).html().replace("SUPERDUPER_POSTFIX_REMOVE_THIS_IMMEDIATELY", ""));
								}
							});
						}
						if ($(this).find("span:contains(SUPERDUPER_TAILORED_START_REMOVE_THIS_IMMEDIATELY)").length)
						{
							$(this).find("td:nth-child(2) span").each(function() 
							{
								if ($(this).html().indexOf("SUPERDUPER_TAILORED_START_REMOVE_THIS_IMMEDIATELY") != -1)
								{
									//changed_lines.push($(this).attr("id"));
									go_orange = true;
									$(this).html($(this).html().replace("SUPERDUPER_TAILORED_START_REMOVE_THIS_IMMEDIATELY", ""));
								}

								if (go_orange)
								{
									$(this).addClass("tailored_line");
								}

								if ($(this).html().indexOf("SUPERDUPER_TAILORED_END_REMOVE_THIS_IMMEDIATELY") != -1)
								{
									go_orange = false;
									$(this).html($(this).html().replace("SUPERDUPER_TAILORED_END_REMOVE_THIS_IMMEDIATELY", ""));
								}
							});
						}
					});

					if (changed_lines.length)
					{
						var hop_index = 0;
						$(window).scrollTop($('#'+changed_lines[hop_index]).offset().top);
						/*$(window).keydown(function(event) {
							if (event.which ==  32)
							{
								hop_index = hop_index + 1;
								if (hop_index >= changed_lines.length)
								{
									hop_index = 0;
								}
								event.preventDefault();
								$(window).scrollTop($('#'+changed_lines[hop_index]).offset().top);
						
							}
						});*/
					}
				}
			}, "json");
		}
		else
		{
			diff_file.addClass("hidden");
			diff_file.removeClass("visible");
			diff_file.html("");
		}

		if (!$("#readme_container").hasClass("hidden"))
		{
			$("#readme_container").addClass("hidden");
		}
		else if (!$(".diff_file.visible").length)
		{
			$("#readme_container").removeClass("hidden");	
		}
		
	});

	$(".clear_generations").click(function(e) {
		e.preventDefault();
		var trigger = $(this);
		var version = this.id;
		var params = {
			action: "ClearGenerations",
			caller: trigger.parent().find(".caller").text(),
			version: version
		};
		$.post(window.location, params, function(data) {
			if (data.success)
			{
				location.reload();
			}
			else if (data.message)
			{
				trigger.parent().find(".notice").html(data.message);	
			}
		}, "json");
	});

});