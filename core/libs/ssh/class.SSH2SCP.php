<?php
/*
    http://www.sitepoint.com/using-ssh-and-sftp-with-php/
*/
class SSH2SCP extends SSH2
{
    public function __call($func, $args) {
        $func = 'ssh2_scp_' . $func;
        if (function_exists($func)) {
            array_unshift($args, $this->conn);
            return call_user_func_array($func, $args);
        }
        else {
            throw new Exception(
                $func . ' is not a valid SCP function');
        }
    }
}