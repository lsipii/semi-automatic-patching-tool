<?php
/*
    http://www.sitepoint.com/using-ssh-and-sftp-with-php/
*/
class SSH2SFTP extends SSH2 {
    protected $sftp;
 
    public function __construct($host, ISSH2Authentication $auth, $port = 22) {
        parent::__construct($host, $auth, $port);
        $this->sftp = ssh2_ftp($this->conn);
    }
    public function __call($func, $args) {
        $func = 'ssh2_sftp_' . $func;
        if (function_exists($func)) {
            array_unshift($args, $this->sftp);
            return call_user_func_array($func, $args);
        }
        else {
            throw new Exception(
                $func . ' is not a valid SFTP function');
        }
    }
}