<?php
/*
    http://www.sitepoint.com/using-ssh-and-sftp-with-php/
*/
class SSH2Password extends SSH2Authentication
{
    protected $username;
    protected $password;
 
    public function __construct($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }
 
    public function getUsername() {
        return $this->username;
    }
 
    public function getPassword() {
        return $this->password;
    }
}