<?php
/*
    http://www.sitepoint.com/using-ssh-and-sftp-with-php/
*/
class SSH2
{
    protected $conn;
 
    public function __construct($host, SSH2Authentication $auth, $port = 22) {
        $this->conn = ssh2_connect($host, $port);
        switch(get_class($auth)) {
            case 'SSH2Password':
                $username = $auth->getUsername();
                $password = $auth->getPassword();
                if (ssh2_auth_password($this->conn, $username, $password) === false) {
                    throw new Exception('SSH2 login is invalid');
                }
                break;
            case 'SSH2Key':
                $username = $auth->getUsername();
                $publicKey = $auth->getPublicKey();
                $privateKey = $auth->getPrivateKey();
                if (ssh2_auth_pubkey_file($this->conn, $username, $publicKey, $privateKey) === false) {
                    throw new Exception('SSH2 login is invalid');
                }
                break;
            default:
                throw new Exception('Unknown SSH2 login type');
        }
    }
}