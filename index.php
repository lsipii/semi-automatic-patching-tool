<?php		
session_start();	
require("core/classes/class.Config.php");
require("core/classes/class.Transfer.php");
require("core/classes/class.UpgradeStuff.php");
require("core/classes/class.GenerateStuff.php");
//require("core/classes/class.InitialPatch.php");
require("core/classes/class.AjaxHandler.php");
$ajax = new AjaxHandler();

?>
<html>
<head>
<meta charset='utf8'>
<link type="text/css" rel="StyleSheet" href="core/css/upgrade.css"/>
<link type="text/css" rel="StyleSheet" href="core/css/view.css"/>
</head>

<body>
	<div id='wrapper'>
	<div id='toggle_readme'>Toggle readme visibility</div>

	<div id='tool_container'>
	<div id='tools'>
	<div id='nav'>
		<?php
			
			if (isset($_REQUEST['view']))
			{
				$_SESSION['view'] = $_REQUEST['view'];
			}
			
			if (!isset($_SESSION['view']))
			{
				$_SESSION['view'] = "initial";
			}
			$view = $_SESSION['view'];
			echo "<a id='initial_view' ".($view == "initial" ? "class='selected' " : "")." href=''>Initial patch</a>";
			echo "<a id='generate_view' ".($view == "generate" ? "class='selected' " : "")." href=''>Generate patch</a>";
			echo "<a id='upgrade_view' ".($view == "upgrade" ? "class='selected' " : "")."href=''>Upgrade view</a>";
		?>
	</div>
	<div id='notice'></div>
<?php

// Generate the initial base patch
if ($view == "initial")
{
	echo "<h1>Initial patch:</h1>";
	try 
	{
		GenratePatch(Config::$patches["patch_base_version"], true);
	}
	catch (Exception $e) 
	{
		echo "Exception: ".$e->getMessage()."<br>";
	
	}
}
else
{
	if ($view == "generate")
	{
		echo "<h1>Version specific patches:</h1>";
	}
	if ($view == "upgrade")
	{
		echo "<h1>Tailored version specific patches:</h1>";
	}
	uasort(Config::$shops, 'CompareVersionNumbers');
	$shop_folders = array_keys(Config::$shops);
	$generated_patch_version_views = array();

	foreach ($shop_folders as $key => $shop_folder)
	{
		try 
		{
			switch ($view) 
			{
				case 'generate':
					$current_version = Config::$shops[$shop_folder]['shop_version'];
					if (!in_array($current_version, $generated_patch_version_views))
					{
						$generated_patch_version_views[] = $current_version;
						GenratePatch($current_version);
					}
					break;
				case 'upgrade':
					StartUpgrade($shop_folder);
					break;	
				default:
					# code...
					break;
			}
		} 
		catch (Exception $e) 
		{
			echo "Exception: ".$e->getMessage()."<br>";
			exit;	
		}
	}
}
?>
</div>
</div>
	<div id='readme_container' class='hidden'>
<?php
	$readme_string = file_get_contents("README.md");
	echo FormatPlainTextToHtml($readme_string);
?>
	</div>
</div>
<script type='text/javascript' src="core/js/libs/jquery.js"></script>
<script type='text/javascript' src="core/js/upgrade.js"></script>
<script type='text/javascript' src="core/js/view.js"></script>
</body>
</html>
<?php
function StartUpgrade($current_shop)
{	
	$current_version = Config::$shops[$current_shop]['shop_version'];
	echo "<div class='action_container'>";
	echo "<h2>$current_shop - $current_version</h2>";
	if ($not_good_message = TestVersionCompability($current_version))
	{
		echo $not_good_message;	
	}
	else
	{
		$upgrade_stuff = new UpgradeStuff($current_shop);
		$upgrade_stuff->CreateStuff();
		$upgrade_stuff->DiDeDiffTadleDonDerrorDilesDplease();
		echo "<div style='display: inline;'><a class='clear_generations' id='$current_shop' href=''>Clear folders</a></div>";
	}
	
	echo "<br>DONE";
	echo "</div>";
}
function GenratePatch($current_version, $initial = false)
{	
	echo "<div class='action_container'>";
	echo "<h2>$current_version</h2>";
	
	if ($not_good_message = TestVersionCompability($current_version))
	{
		echo $not_good_message;	
	}
	else
	{
		$generate_stuff = new GenerateStuff($current_version, null, $initial);
		$generate_stuff->CreateStuff();
		$generate_stuff->DiDeDiffTadleDonDerrorDilesDplease();
		echo "<div style='display: inline;'><a class='clear_generations' id='$current_version' href=''>Clear folders</a></div>";	
	}
	echo "<br>DONE";
	echo "</div>";
}
function TestVersionCompability($current_version)
{
	if(!version_compare($current_version, Config::$patches["lowest_base_version"], ">="))
	{
		return "The patch is limited to versions ".Config::$patches["lowest_base_version"]." and above..<br>";
	}
	else if (in_array($current_version, Config::$patches["skip_versions"]))
	{
		return "The version $current_version is marked as to be skipped..<br>";
	}
	return false;
}
function CompareVersionNumbers($a, $b) 
{
	if ($a['shop_version'] == $b['shop_version'])
	{
		return 0;
	}
	return (version_compare($a['shop_version'], $b['shop_version'], ">")) ? -1 : 1;
}
function FormatPlainTextToHtml($readme_string)
{
	$readme_string = htmlspecialchars($readme_string);
	$readme_string = str_replace("\n", "<br>", $readme_string);	
	$readme_string = preg_replace("/(\!\[Image\]\()(.*?)(\))/", "<img src='$2'/>", $readme_string);	
	
	$readme_string = preg_replace('@(https?://(?!lsipii)([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $readme_string);
	$readme_string = preg_replace("/(###)(.*?)(###)/", "<h3>$2</h3>", $readme_string);	
	$readme_string = preg_replace("/(##)(.*?)(##)/", "<h2>$2</h2>", $readme_string);	
	$readme_string = preg_replace("/(#)(.*?)(#)/", "<h1>$2</h1>", $readme_string);	
	$readme_string = preg_replace("/(\*)(.*?)(\*)/", "<b>$2</b>", $readme_string);	
	$readme_string = preg_replace("/(red\:)(.*?)(\:red)/", "<span class='red'>$2</span>", $readme_string);	
	$readme_string = preg_replace("/(green\:)(.*?)(\:green)/", "<span class='green'>$2</span>", $readme_string);	
	$readme_string = preg_replace("/(orange\:)(.*?)(\:orange)/", "<span class='orange'>$2</span>", $readme_string);	
	$readme_string = preg_replace("/(blue\:)(.*?)(\:blue)/", "<span class='blue'>$2</span>", $readme_string);
	$readme_string = preg_replace("/(```)(.*?)(```)/s", "<pre>$2</pre>", $readme_string);
	$readme_string = str_replace("<br>#!xml", "", $readme_string);
	$readme_string = preg_replace("/(<br>&gt;)(.*?)(<br>.<br>)/s", "<div class='tabbed'>$2</div>", $readme_string);
	$readme_string = str_replace("<br>&gt;", "<br>", $readme_string);
	return $readme_string;
}
?>