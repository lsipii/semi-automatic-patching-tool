<?php

// Runtime PHP-settings
ini_set('display_errors', 1);
ini_set('memory_limit', '1624M');

/*
 * General upgrade information
 */
Config::$general_operation_info["upgrade_or_patch"] = "patch"; // values "upgrade" or "patch". //"upgrade"-mode not implemented
Config::$general_operation_info["upgrade_to_version"] = "-hotfix1";
Config::$general_operation_info["author"] = "Lassi";
Config::$general_operation_info["additional_info"] = "Testi";

/*
 * Upgrading process paths 
 */ 
Config::$general_operation_info["base_versions_directory"] = "/home/varasto/prospercart_install/";
Config::$upgrade["working_directory"] = "kaupat";
Config::$patches["working_directory"] = "generated_patches";


// Names of the temporary folders
Config::$general_operation_info["tailored_version_folder"] = "tailored_version";
Config::$general_operation_info["new_version_folder"] = "new_version";
Config::$general_operation_info["upgrade_folder"] = "patched_files";

// Recursive search for files in version control folders
Config::$general_operation_info["recursive_diff"] = true;

// Path between bases versions directory and real files
Config::$general_operation_info["base_filepath_prefix"] = "Files/public_html";

// Upgrade only these folders
//Config::$general_operation_info["version_control_folders"][] = "admin/reports";

// Upgrade only these files
//Config::$general_operation_info["filename_override"][] = "admin/products/product_mass_functionality_ajax.php";

//Config::$general_operation_info["skip_these_files"][] = "admin/content/news_transfer_script.php";



/*
 * Patch versions
 */
Config::$patches["patch_base_version"] = "2.1.1.0";
Config::$patches["lowest_base_version"] = "1.6.1.0";
Config::$patches["skip_versions"] = array();

//Config::$patches["patch_folder"] = "patch/public_html";


/*
 * Specific shop information
 */

/*
FORMAT:
$kauppa = "kauppa.marjatta.prospercart.fi";
Config::$shops[$kauppa]['shop_version'] = "2.0.2.2";
Config::$shops[$kauppa]['hostname'] = "kauppa.marjatta.prospercart.fi";
Config::$shops[$kauppa]['username'] = "kauppa_tunnari";
Config::$shops[$kauppa]['password'] = "kauppa_salasana";
//Config::$shops[$kauppa]['public_key'] = "";
//Config::$shops[$kauppa]['private_key'] = "";
Config::$shops[$kauppa]['prospercart_admin_folder'] = "hallinnan_kansionimi";
Config::$shops[$kauppa]['public_html_folder'] = "web/prospercart";
*/
include("tunnukset.php");