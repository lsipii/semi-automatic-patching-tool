<?php
	$readme_string = file_get_contents("test.txt");

	$readme_string = htmlspecialchars($readme_string);
	$readme_string = str_replace("\n", "<br>", $readme_string);	
	$readme_string = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $readme_string);
	$readme_string = preg_replace("/(###)(.*?)(###)/", "<h3>$2</h3>", $readme_string);	
	$readme_string = preg_replace("/(##)(.*?)(##)/", "<h2>$2</h2>", $readme_string);	
	$readme_string = preg_replace("/(#)(.*?)(#)/", "<h1>$2</h1>", $readme_string);	
	$readme_string = preg_replace("/(\!\[Image\]\()(.*?)(\))/", "<img src='$2'/>", $readme_string);	
	$readme_string = preg_replace("/(\*)(.*?)(\*)/", "<b>$2</b>", $readme_string);	
	$readme_string = preg_replace("/(red\:)(.*?)(\:red)/", "<span class='red'>$2</span>", $readme_string);	
	$readme_string = preg_replace("/(green\:)(.*?)(\:green)/", "<span class='green'>$2</span>", $readme_string);	
	$readme_string = preg_replace("/(orange\:)(.*?)(\:orange)/", "<span class='orange'>$2</span>", $readme_string);	
	$readme_string = preg_replace("/(blue\:)(.*?)(\:blue)/", "<span class='blue'>$2</span>", $readme_string);
	$readme_string = preg_replace("/(&lt;code&gt;)(.*?)(\&lt;\/code&gt;)/s", "<pre>$2</pre>", $readme_string);
	$readme_string = preg_replace("/(<br>\t&gt;)/", "<br>", $readme_string);
	
	$readme_string = preg_replace("/(<br>&gt;)(.*?)(<br>.<br>)/s", "<div class='tabbed'>$2</div>", $readme_string);
	$readme_string = str_replace("<br>&gt;", "<br>", $readme_string);
	

echo "<html>".$readme_string."</html>";
?>