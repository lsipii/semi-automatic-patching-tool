# README #


### What is this repository for? ###

With the semi-automatic patching tool you can generate and distribute software bug-fixes to numerous of software instances that have different software versions and tailored source codes.

The tool makes the hard lifting of a software upgrade and lets the updater focus on the most important task that is trying not to mess up the customers tailored functions in the upgrade progress.

The upgrading process can usually be boldy described in these steps:

1. make the patch 
2. compare customers software to the base version 
3. copy the differences from the comparison to the patch 
4. apply the patch to the customers software.

This is a simple enough task when the update affects only a few instances of the software. The problem comes when the bug affection level is more broad. An update to one version may not work on a different one and the affected files migth be heavily modified by the customer and needs extra consideration. Because of this uniquenes the update process is complex and time consuming and that way prone to errors.

Semi-automatic patching tool tries to simplify the process by automaticly generating version specific patches and applying those to the customers tailored version and then transfering the files between the patch workspace and customers production server. 

A simple demonstration can be found here: https://lsipii.kapsi.fi/upgrade/

::: 

The patch is generated to the version specific patches in the following steps:

- the patch files are compared to the base version of the files
- the differences in code are transformed to a vQmod search and replace rulesets
- the generated rules are applied to version specific patch files

The results of the run are displayed to the user for evaluation. If the version specific patch could not be generated for example for refactoring reasons the user will be instructed to either to fix the problem manually for the file or create a new patch that will be used for that and lower/higher versions.


Applying the patch to the customers version is done the following steps:

- customer versions relevant files are copied from the production server to the working directory
- the files are compared to the base version
- the differences in code are transformed to a vQmod search and replace rulesets
- the generated rules are applied to the version specific patch files
- the patch is uploaded to customers production version.



Used tools:

* vQmod, https://github.com/vqmod/vqmod/wiki
* PHP Diff implementation, http://code.stephenmorley.org/php/diff-implementation/
* phpseclib: http://phpseclib.sourceforge.net/


-------
### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact