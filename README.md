# Puoliautomaattinen ohjelmiston paikkaustyökalu #
**Korjauspäivitysten jakaminen versiokohtaisiin ja räätälöityihin tuotantoympäristöihin**

Opinnäytetyön seminaariesitys 
5.12.2014


Lassi Piironen

Tampereen ammattikorkeakoulu
Tietotekniikka
Ohjelmistotekniikka

## Taustat ja ongelman kuvaus ##

**Ohjelmistopäivitysprosessien tulee useimmiten huomioida ohjelmiston paikalliset muutokset**
- Esimerkiksi ohjelman käyttäjäasetuksia tai suurempia kokonaisuuksia itse ohjelmiston lähdekoodissa 
 
**Muutosten päivitettävyys on arkkitehtuurikysymys** 
- Jos systeemissä on erilaisia räätälöintejä varten suunniteltu plugin-toteutus, on se usein myös helposti päivitettävä
- Jos päivitettävät kohdeympäristöt on liitetty versionhallintajärjestelmään pitäisi paikallisten muutosten olla jo tiedossa

Kun ei olla varmoja siitä että kaikki kohdejärjestelmän muutokset olisivat tiedossa, täytyy tehdä tiedostovertailut
järjestelmän version alku- ja lopputilanteiden välillä.

**Korjauspäivitysten jakaminen suurelle määrää tuotantoversioita on hankalaa** 
- Jokainen päivitettävä instanssi vaatii oman räätälöidyn korjauspäivityksen. 

### Tuotantoversiot suhteessa kehitysympäristöön ###


Työn taustalla olevat ohjelmiston kehitysympäristöön liittyvät tuotantoympäristöt 
ovat usein hyvin raskaasti räätälöityjä ja pohjautuvat ohjelmiston nykyisestä uusimmasta versiosta paljon
vanhempiin versioihin. 

Versionhallintaan tallennetaan aina tuotantoon julkaistujen versioiden alkutilanne. 

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/1.png)


### Ohjelmistosuunnittelija luo ohjelmistoon tärkeän korjauksen ###

Kun ohjelmistosta löydetään virhe, luodaan siihen vikatilanteen korjaava paikkaus. 
Alla olevassa kuvassa paikkausta esitetään nimellä "patch1". 

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/2.png)


### Luotu korjaus ei sovi sellaisenaan eri tuotantoversiohin  ###

Kun korjaus on tehty eri versioon mitä tuotannossa on käytössä, ei 
voida olla aivan varmoja siitä, että paikkaus toimisi lopulta muualla. 

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/3.png)


### Korjauksesta pitää tehdä eri versiolle sopivat korjaukset  ###

Korjauksen pohjalta voidaan tehdä uudet eri versiolle sopivat korjaukset,
mutta edelleenkään ei ole varmuutta siitä, toimiiko versiokohtaiset korjaukset niissä tuotantoympäristössä joihin on tehty paikallisia muutoksia.

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/4.png)

### Versiokohtaiset korjaukset pitää muuntaa tuotantoympäristölle sopivaksi ###

Paikkauksen pitävyydestä voidaan olla varmoja vasta silloin, kun alkuperäisestä paikkauksesta on tehty
versio joka on sovitettu kohdeympäristön sekä ohjelmistoversioon että sen paikallisiin muutoksiin.

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/5.png)


### Kun tuotantoympäristöjä on paljon, on päivitysprosessi raskas ###

Yksittäiselle tuotantoversion korjauspäivitykselle ohjelmiston päivittäjä:

- luo versiokohtaisen bugikorjauksen vertailemalla tiedostomuutoksia uusimman ja korjatun version välillä ja siirtämällä muutokset uuteen versikohtaiseen päivityspakettiin
- luo tuotantoympäristökohtaisen bugikorjauksen vertailemalla muutoksia tuotantoversion alku- ja lopputilanteen välillä ja siirtämällä olennaiset eroavaisuudet uuteen päivityspakettiin
- asentaa päivityksen tuotantoympäristöön huolehtien varmuuskopioista, siirtelemällä tiedostoja ja tekemällä päivitykseen liittyvät testaukset.


![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/6.png)


## Opinnäytetyön tavoitteet ##

Työn tavoitteena oli luoda järjestelmä millä pienten korjausten laajamittainen levittäminen olisi asentajan näkökulmasta edes suhteellisen helppoa. 

Työkalun tarkoitus oli vähentää asentajan työtaakkaa automatisoimalla pohja- ja siirtotöitä siten että asentajan pääasiallinen tehtävä olisi luotujen korjausten arvioiminen ja hyväksyminen.

Ideaali päivitysprosessi olisi tavoitteiden toteutuessa seuraavanlainen:

- ohjelmoija luo korjauksen ohjelmiston uusimpaan kantaversioon
- asentaja asettaa työkalun asetukset ja lisää korjauksen työkaluun
- työkalu luo korjauksesta kantaversiokohtaiset korjaukset
- asentaja tarkastaa että korjaukset muodostuivat oikein
- työkalu luo kantaversikohtaisista korjauksista asiakaskohtaiset korjaukset
- asentaja tarkastaa että korjaukset muodostuivat oikein
- asentaja hyväksyy päivityksen
- työkalu asentaa päivityksen tuotantoversioon.

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/7.png)


## Työssä toteutetun työkalun näkymät ##

Esittelyssä on kaksi näkymää: 

- laastarin luontinäkymä "Generate patch" 
- tuotantoversion päivitysnäkymä "Upgrade view". 

Molemmat näkymät listaavat kyseisen päivityksen tiedostot. Tiedoston nimeä klikkaamalla avautuu tiedoston vertailunäkymä.

Vertailunäkymässä esitetään kahden eri tiedoston sisällöt vierekkäin ja merkitään eroavaisuudet värikoodeilla:

- red:punainen::red vanha rivi
- green:vihreä::green uusi rivi
- blue:sininen::blue generoinnissa muuttunut rivi.
- orange:oranssi::orange generoinnissa räätälöidyksi riviksi tunnistettu uusi rivi.

Esittelyn vertailunäkymien tiedostoversioden nimeämiskäytäntö on seuraava: 

- **Base** <versionumero>: kyseinen kantaversio, tai tuotantoversion alkutilanne
- **Tailored** <versionumero>: räätälöity tuotantoversio, lopputilanne
- **Patched** <versionumero>-<laastarin nimi>: versiokohtainen laastaroitu versio
- **Generated** <versionumero>-<laastarin nimi>: työkalun generoima versiokohtainen ja räätälöity versio
- **Upgraded** <versionumero>-<laastarin nimi>: tallennettu ja hyväksytty laastari valmiina ladattavaksi tuotantoon.


### Laastarin luontinäkymä ###

Ohjelmistopäivityksen versiokohtaisten tiedostojen luominen toteutetaan työkalussa seuraavin askelin:

- tehdään vertailu päivityksen tiedostojen ja kyseisen kantaversion vastaavien tiedostojen välillä
- muunnetaan vertailun eroavaisuudet vQmod-kirjaston haku- ja korvaus-säännöiksi 
- ajetaan vQmod-säännöt kantaversiota vanhempiin kantaversioihin 
- näytetään säännöistä generoidut versiokohtaiset päivitystiedostot päivittäjälle vertailumuodossa.

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/9.png)


Asentaja käy läpi generoidut päivitystiedostot ja joko hyväksyy generoidut muutokset tai korjaa ongelmat käyttäen apukeinona työkalun ilmoittamia virheilmoituksia.


Esittelysivulla muuntamisprosessi esitetään tunnisteilla seuraavasti: 

Työkalu tekee vertailun:

- kyseisen kantaversion tiedoston ja päivityksen tiedoston välillä
  	Tiedostojen tunnisteet: **Base** <versionumero> ja **Patched** <päivityksen kantaversionumero>

- kyseisen kantaversion tiedoston ja siitä että päivityksen tiedostosta generoidun tiedoston välillä
  	Tiedostojen tunnisteet: **Base** <versionumero> ja <versionumero>-<laastarin nimi>

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/patch_generation_shot.png)

### Tuotantoversion päivitysnäkymä ###

Versiokohtainen ohjelmistopäivitys asennetaan tuotantoversioon seuraavin askelin:

- kopioidaan päivitykseen liittyvät tiedostot tuotantoversiosta päivitystyökalun asennuskansioon
- tehdään vertailu tuotannon tiedostojen ja kyseisen kantaversion välillä
- muunnetaan vertailun eroavaisuudet vQMod-kirjaston haku- ja korvaus-säännöiksi
- ajetaan luodut vQmod-säännöt kyseisen version päivitystiedostoihin
- näytetään säännöistä generoidut versio- ja räätälöintikohtaiset päivittäjälle vertailumuodossa.


![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/10.png)



Työkalu jättää asentajan tehtäväksi arvioida onnistuiko tuotantoversion räätälöintien automaattinen siirto uuteen versioon.

Kun asentaja on hyväksynyt versio- ja räätälöintikohtaisen päivityksen ladataan päivitystiedosto tuontantoversioon.


Esittelysivulla muuntamisprosessi esitetään tunnisteilla seuraavasti: 

Työkalu tekee vertailun:

- kyseisen kantaversion ja tuotantoversion tiedoston välillä
  	Tiedostojen tunnisteet: **Base** <versionumero> ja **Tailored** <versionumero>

- laastarin ja tuotantoversion tiedoston välillä
  	Tiedostojen tunnisteet: **Patched** <versionumero>-<laastarin nimi> ja **Tailored** <versionumero>

- laastarin ja tuotantoversion sekä laastarin tiedostoista generoidun tiedoston välillä
  	Tiedostojen tunnisteet: **Patched** <versionumero>-<laastarin nimi> ja **Tailored** <versionumero>

- tuotantoversion tiedoston ja päivittäjän hyväksymän ja tallentaman tiedoston välillä
  	Tiedostojen tunnisteet: **Tailored** <versionumero> ja **Upgraded** <versionumero>-<laastarin nimi>


![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/upgrade_view_shot.png)



## Toteutuksesta ##

Työkalun perustoiminnot on toteutettu PHP-ohjelmointikielellä. Käyttöliittymän toteutukessa on
on käytetty yleisimpiä www-ohjelmoinnin tekniikoita ja työkaluja kuten jQuery JavaScript-kirjasto. 

Työkalun olennaisimmat toiminnot liittyvät kerrallaan kahden tiedoston rivi-riviltä vertailemiseen 
ja vertailuista löydettyjen muutosten kolmanteen tiedostoon siirtämiseen. 

![Image](https://lsipii.kapsi.fi/upgrade/readme_pics/8.png)

### Version tiedostojen lävitse käyminen sekä vertailu ###

Työkalu käy lävitse versioiden tiedostorakenteet ja tekee sen tiedostoille vertailut jossa etsitään eri tiedostoriveiltä versioiden välisiä poikkeamia. 

Vertailu on toteutettu Stephen Morleyn toteuttamalla "PHP Diff implementation" -tiedostovertailukirjastolla.

Kirjaston compareFiles-metodille annetaan parametriksi kahden eri tiedoston tiedostopolut ja se antaa paluuarvona tiedostojen rivien erot ja yhteneväisyydet kaksiulotteisessa listamuodossa. 

Morleyn compareFiles-metodin palauttaman listan sisältää tiedoston rivien seuraavat muutostilat:

- rivi on muuttumaton
- rivi on poistettu
- poistettu rivi on korvattu uudella rivillä
- rivi on lisätty. 


### Muutosten siirtäminen kolmanteen tiedostoon ###


Rivikohtaisten muutostietojen perusteella työkalu luo vQmod-kirjaston syntaksin mukaisia haku- ja korvaus-sääntöjä. 


>vQmod-kirjasto on alunperin avoimen lähdekoodin OpenCart-verkkokauppaohjelmistolle kehitetty plugin-systeemi jolla erilaisia ohjelmiston räätälöintejä on mahdollista ottaa käyttöön muokkaamatta itse PHP-ohjelmiston lähdekoodeja. 
>
>Systeemi toimii niin että räätälöinnin tiedostomuutokset kirjoitetaan kirjaston syntaksin mukaiseen XML-muotoon joista kirjasto luo ajonaikaisesti ns. väliaikaiset lähdekoodiversiot. Jos kirjasto on integroitu osaksi ohjelmistoa, käytetään ohjelmiston suorittamiseen alkuperäisten lähdekooditiedostojen sijasta näitä geneituja versioita.

Työkalu käyttää tästä kirjastosta vain sen generointiominaisuuksia siten että tiedostovertailuista löydettyjen muutosten perusteella luodaan generaattorilla kolmas tiedosto jossa muutokset on ajettu kyseisen päivityspaketin vastaavaan tiedostoon. 

Siten esimerkiksi räätälöidyn tuotantoversion ja sen alkutilanteen vertailussa löydetyt muutokset saadaan ajettua
kyseisen päivityspaketin vastaaviin tiedostoihin. 

Esimerkki VQModXMLGenerator-luokan tuottamasta XML-syntaksista:


```
#!xml
<!-- Generated by VQModXMLGenerator : Temporary file -->
<modification>
	<id><![CDATA[1.5.2.4_1.5.2.4-patch1-1]]></id>
	<version><![CDATA[1.5.2.4_1.5.2.4-patch1]]></version>
	<vqmver><![CDATA[2.4.1_up]]></vqmver>
	<author><![CDATA[Lassi]]></author>
	<file name="new_version\moi.php" path="">
		<operation info="Upgrade: 1.5.2.4_1.5.2.4-patch1-1 :: moi.php @LassiTesti">
			<search position="replace" trim="false" index="1" >
				red:<![CDATA[	echo "kauppahan se";]]>:red
			</search>
			<add trim="false">
				green:<![CDATA[	echo "kauppahan se, kauppa mikä kauppa.";]]>:green
			</add>
		</operation>
	</file>
</modification>
<!-- Generated by VQModXMLGenerator : Temporary file -->
```

## Pohdintoja ##


Jos ohjelmiston kehitysympäristö on rakennettu siten että tuotantoympäristöjen paikalliset muutokset on aina kehitysympäristön tiedossa, on suuri osa päivitysongelmista jo ratkaistu. Versio- ja ympäristökohtaiset korjauspäivitykset voidaan tällöin tehdä suoraan versionhallinnastatyökaluilla. Korjauksen revisiot yhdistetään (mergataan) uusimmasta versiosta tuotantoympäristön omaan haaraan (branchiin) ja versionhallinta pitää huolen versiokohtaisuudesta ja kun paikalliset muutokset on mukana versiossa on sekin puoli hoidettu. 

Kuitenkin jos tälläistä optimaalista tilannetta ei ole ja kehitysympäristö ei "tiedä" mitä tuotannossa tapahtuu,
ajaa opinnäytetyön tuotos asiansa pikaisten korjausten levittämisessä.  Tällöinkin työkalu pitäisi yhdistää
kehitysympäristön paikalliseen versionhallintaan. Opinnäytetyön tuotos ei ole yksittäisenä työkaluna kokonainen vastaus
päivitysarkkitehtuurin ongelmiin.

Hyvin toteutettuun kehitysympäristöön työkalulla ei sellaisenaan ole paljoa annettavaa. Tämänlaisiin systeemeihin työkalua voisi kuitenkin kehittää versionhallinnan apuvälineeksi esim. siten että versionhallinan päivitystoimintoja olisi mahdollista tehdä moniajoina. 

Työssä käytetyllä vQmod-kirjastolla olisi myös mahdollista toteuttaa monenmoista kuten:

- isompien päivitysten virtuaalinen testaaminen tuotantoympäristössä:
	jos ohjelmaa käytettäisiin tietystä ip-avaruudesta ajettaisiin tuotantoversion sijaan vQmod:n välimuistitiedostot,
	tietokantamuutosten osalta virtuaalisuus voitaisiin saavuuttaa esim. PDO -tekniikalla käyttäen SQLite3 -tauluja

-  paikallisten muutosten modularisoiminen:
	vQmod on tehty alunperin plugin -toiminnallisuuksia mahdollistavaksi kirjastoksi. Opinnäytetyön tuotoksen ajatuksella olisi mahdollista muuntaa paikalliset muutokset XML-säännöiksi jolloin paikalliset toiminnalliset muutokset olisi mahdollista eriyttää ohjelmiston lähdekoodista omiksi yksiköikseen. 


## Työssä käytetyt työkalut ##

Työssä on käytetty seuraavia työkaluja:

- vQmod 2.4.1, https://github.com/vqmod/vqmod/wiki
- PHP Diff implementation, http://code.stephenmorley.org/php/diff-implementation/
- phpseclib, http://phpseclib.sourceforge.net/

vQmod-kirjastoa on muokattu siten että sitä voi ajaa useampaan eri tiedostopolkuun ja sen aiheuttamat virheviestit saadaan kaapattua kutsuvan olioluokan myöhempää käyttöä varten.